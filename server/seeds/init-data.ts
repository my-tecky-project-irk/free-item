import * as Knex from "knex";
import { hashPassword } from "../hash";

const userTable = "users";
const catsTable = "categories";
const itemsTable = "items";
const roomsTable = "rooms";
const msgsTable = "messages";
const forumTable = "forum";
const socialPlatformTable = "social_platforms";
const commentTable = "comment";
const favorTable = "favor";
const imgTable = "image"

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(imgTable).del();
    await knex(favorTable).del();
    await knex(commentTable).del();
    await knex(msgsTable).del();
    await knex(roomsTable).del();
    await knex(itemsTable).del();
    await knex(forumTable).del();
    await knex(catsTable).del();
    await knex(socialPlatformTable).del();
    await knex(userTable).del();

    const hashedPassword = await hashPassword("123");

    // Inserts seed entries
    const userId = await knex(userTable)
        .insert([
            {
                username: "roger@taketwo.com",
                display_name: "Roger",
                password: hashedPassword,
                picture: "https://server.taketwo.shop/uploads/wolf.jpg",
                lat: "22.3",
                lng: "114.0",
            },
            {
                username: "iris@taketwo.com",
                display_name: "Iris",
                password: hashedPassword,
                picture: "https://server.taketwo.shop/uploads/tiger.jpg",
                lat: "22.3",
                lng: "114.0",
            },
            {
                username: "karl@taketwo.com",
                display_name: "Karl",
                password: hashedPassword,
                picture: "https://server.taketwo.shop/uploads/fox.jpg",
                lat: "22.3",
                lng: "114.0",
            },
        ])
        .returning("id");

    const catsId = await knex(catsTable)
        .insert([
            { category: "Electronics", is_forum: false },
            { category: "Fashion", is_forum: false },
            { category: "Daily Essentials", is_forum: false },
            { category: "Babies&Kids", is_forum: false },
            { category: "Furnitures", is_forum: false },
            { category: "Others", is_forum: false },
            { category: "Q&A", is_forum: true },
            { category: "Tips & Tricks", is_forum: true },
            { category: "Events", is_forum: true },
            { category: "Announcement", is_forum: true },
            { category: "Food Rescue", is_forum: true },
        ])
        .returning("id");

    await knex(forumTable).insert([
        {
            user_id: userId[1],
            title: "節約水資源",
            category_id: catsId[7],
            description:
                "洗澡儘量不盆浴，而是淋浴。擦洗法液或沐浴露時要關水龍頭，不然會把水浪費掉的。洗完後立刻關掉水龍頭。家中備幾個污水桶，將各種洗滌用水積攢起來用於沖洗廁所。",
            hash_tag: JSON.stringify([]),
        },
        {
            user_id: userId[1],
            title: "珍惜森林",
            category_id: catsId[7],
            description:
                "廢紙是我們生活中，浪費最嚴重的資源。平時使用後的紙張，報紙，書刊，建議都進行分類，用過一面的紙可以翻過來做草稿紙、便條紙，或自製成筆記本使用；過期的掛曆可以包書皮。",
            hash_tag: JSON.stringify([]),
        },
        {
            user_id: userId[1],
            title: "慎用清潔劑",
            category_id: catsId[7],
            description:
                "減少水污染，拒絕含磷洗滌劑，只用無磷洗滌劑。若時間允許、體力允許，儘量手洗衣服，既節電又節水。",
            hash_tag: JSON.stringify([]),
        },
        {
            user_id: userId[2],
            title: "別把廢電池當塑膠！回收做得好，環境危機也能變商機",
            category_id: catsId[7],
            description:
                "廢乾電池若回收處理不當，內含的重金屬、電解液等，將對環境造成污染。臺灣廢乾電池回收方便，更常提供回收折抵購物金或兌換物品的優惠措施鼓勵民眾回收，成效不錯。不過，廢乾電池有哪些用途呢？讓我們一起來看看廢乾電池如何被妥善回收，重獲新生命！。",
            hash_tag: JSON.stringify([]),
        },
        {
            user_id: userId[2],
            title: "尋找在廢資訊物品中的貴重金屬",
            category_id: catsId[6],
            description:
                "廢資訊物品回收進場後，會先被拆解，分解為IC板等電子廢料、銅、鐵、鋁、塑膠。其中，IC板等電子廢料，進一步經粉碎、分選後，將分離出玻璃纖維樹脂粉、銅鐵、錫、鉛等金屬。最後，CPU處理器經精煉程序後，將可得出金、銀、鈀等貴金屬。",
            hash_tag: JSON.stringify([]),
        },
        {
            user_id: userId[2],
            title: "傳統燈泡，廢照明光源回收處理不同！",
            category_id: catsId[7],
            description:
                "在回收廢照明光源的同時，若有一開始購買燈泡、燈管的紙盒，就應再次放回紙盒、或用舊衣物、報紙等，再交付給辛苦的清潔隊員、照明光源販賣店家或環保機關核可的回收商、回收點進行回收。",
            hash_tag: JSON.stringify([]),
        },
        {
            user_id: userId[1],
            title: "重複使用的耐用品",
            category_id: catsId[6],
            description:
                "用可重複使用的容器裝冰箱裏的食物，而儘量不用一次性的塑料保鮮膜；使用可換芯的圓珠筆，不用一次性的圓珠筆；出外遊玩時自帶水壺，減少塑料垃圾的產生；旅遊或出差時，自帶牙刷等衛生用具，不使用旅館每日更換的牙具等。",
            hash_tag: JSON.stringify([]),
        },
    ]);

    const itemId = await knex(itemsTable)
        .insert([
            {
                creater_id: userId[0],
                category_id: catsId[1],
                title: "tops",
                expire_day: new Date("2020-10-09"),
                intro: "90% New",
                pickup_times: "Mon-Fri",
                listing_days: 10,
                is_available: true,
            },
            {
                creater_id: userId[1],
                category_id: catsId[0],
                title: "mobile",
                expire_day: new Date("2020-10-09"),
                intro: "90% New",
                pickup_times: "Mon-Fri",
                listing_days: 10,
                is_available: true,
            },
            {
                creater_id: userId[2],
                category_id: catsId[4],
                title: "sofa",
                expire_day: new Date("2020-10-09"),
                intro: "90% New",
                pickup_times: "Mon-Fri",
                listing_days: 10,
                is_available: true,
            },
        ])
        .returning("id");

    const roomId = await knex(roomsTable)
        .insert([
            { host_id: userId[0], user_id: userId[1], item_id: itemId[0] },
            { host_id: userId[0], user_id: userId[2], item_id: itemId[0] },

            { host_id: userId[1], user_id: userId[2], item_id: itemId[1] },
            { host_id: userId[1], user_id: userId[0], item_id: itemId[1] },

            { host_id: userId[2], user_id: userId[1], item_id: itemId[2] },
            { host_id: userId[2], user_id: userId[0], item_id: itemId[2] },
        ])
        .returning("id");

    await knex(msgsTable)
        .insert([
            {
                room_id: roomId[0],
                sender_id: userId[1],
                receiver_id: userId[0],
                message: "hi roger, iam iris",
                is_read: false,
            },
            {
                room_id: roomId[0],
                sender_id: userId[0],
                receiver_id: userId[1],
                message: "ysss iris, im roger",
                is_read: false,
            },
            {
                room_id: roomId[1],
                sender_id: userId[2],
                receiver_id: userId[0],
                message: "hi roger, iam karl",
                is_read: false,
            },
            {
                room_id: roomId[1],
                sender_id: userId[0],
                receiver_id: userId[2],
                message: "ysss karl, im roger",
                is_read: false,
            },

            {
                room_id: roomId[2],
                sender_id: userId[2],
                receiver_id: userId[1],
                message: "hi iris, iam karl",
                is_read: false,
            },
            {
                room_id: roomId[2],
                sender_id: userId[1],
                receiver_id: userId[2],
                message: "ysss  karl, iam iris",
                is_read: false,
            },
            {
                room_id: roomId[3],
                sender_id: userId[0],
                receiver_id: userId[1],
                message: "hi iris , iam roger",
                is_read: false,
            },

            {
                room_id: roomId[4],
                sender_id: userId[1],
                receiver_id: userId[2],
                message: "hi karl, iam iris",
                is_read: false,
            },
            {
                room_id: roomId[5],
                sender_id: userId[0],
                receiver_id: userId[2],
                message: "hi karl, iam roger",
                is_read: false,
            },
            {
                room_id: roomId[5],
                sender_id: userId[2],
                receiver_id: userId[0],
                message: "yss roger, iam karl",
                is_read: false,
            },
        ])
        .returning("id");

    await knex(imgTable).insert([
        {
            item_id: itemId[0],
            path: "fasion4.jpg",
        },
        {
            item_id: itemId[1],
            path: "electronics3.jpg",
        },
        {
            item_id: itemId[2],
            path: "furnitures5.jpg",
        },
    ]);
}
