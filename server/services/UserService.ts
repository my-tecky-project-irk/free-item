import Knex from 'knex';
import { tables } from './tables';
import { User } from './models';

export class UserService {
    constructor(private knex: Knex) { }

    createAccount = async (display_name: string, username: string, password: string, picture: string) => {
        const user = await this.knex(tables.USERS).insert({
            username: username,
            display_name: display_name,
            password: password,
            picture: picture,
        }).returning('id')
        return user;
    }

    getUserByUsername = async (username: string) => {
        const user = await this.knex<User>(tables.USERS).where('username', username).first();
        return user;
    };

    getUserByDisplayName = async (display_name: string) => {
        const user = await this.knex<User>(tables.USERS).where('display_name', display_name).first();
        return user;
    };

    getUserByUserID = async (id: number) => {
        const user = await this.knex<User>(tables.USERS).where('id', id).first();
        return user;
    };

    getUserBySocialID = async (socialType: string, socialID: number) => {
        const user = await this.knex(tables.USERS).select(`${tables.USERS}.*`)
            .innerJoin(tables.SOCIAL_PLATFORMS, `${tables.USERS}.id`, '=', `${tables.SOCIAL_PLATFORMS}.user_id`)
            .where(`${tables.SOCIAL_PLATFORMS}.platform_type`, socialType)
            .andWhere(`${tables.SOCIAL_PLATFORMS}.platform_id`, socialID)
            .first();

        return user;
    }

    createUserWithSocial = async (username: string, display_name: string, password: string, socialType: string, socialID: string, picture: string) => {
        const trx = await this.knex.transaction();
        try {
            const [id] = await trx(tables.USERS)
                .insert({
                    username: username,
                    display_name: display_name,
                    password: password,
                    picture: picture
                })
                .returning('id');
            await trx(tables.SOCIAL_PLATFORMS).insert({
                user_id: id,
                platform_type: socialType,
                platform_id: socialID,
            });
            await trx.commit();
            return id as number;
        } catch (err) {
            await trx.rollback();
            throw err;
        }
    };

    getProfileInfo = async (user_id: string) => {
        const user = await this.knex(tables.USERS)     
            .where('id', user_id).first();
        return user;
    }
    
    addForumLike = async (user_id: string, discuss_id: string) => {
        const id = await this.knex(tables.FAV)
            .insert({
                user_id: user_id,
                discuss_id: discuss_id,
                isforumliked: true,
            }).returning('discuss_id')
        console.log(id)
        // const data = await this.knex(tables.FAV)
        // .select("username", "display_name", "password", "created_at")
        // .join(`${tables.FAV}.id`,`=`,`${tables.USERS}.id`)
        // .where(`${tables.FAV}.discuss_id`, id)

        return id;
    }

    async setLocation(userId: number, lat: string, lng: string) {
        const newLocation = await this.knex('users').where('id', userId)
            .update({lat:lat,lng:lng},).returning(['lat','lng'])
        return newLocation 
    }

}
