import Knex from 'knex';
import { tables } from './tables';

export class ForumService {
  constructor(private knex: Knex) { }
  addSingleDiscussion = async (user_id: number, title: string, category_id:number, description: string, hashTag: string, image:string) => {
    const id = await this.knex(tables.FORUM)
      .insert({
        user_id: user_id,
        title: title,
        category_id: category_id,
        description: description,
        hash_tag: hashTag,
        image: image,
      }).returning("id")

    const data = await this.knex(tables.FORUM)
      .select(
        `${tables.FORUM}.user_id`,
        `${tables.FORUM}.title`,
        `${tables.CATEGORIES}.category`,
        `${tables.FORUM}.description`,
        `${tables.FORUM}.hash_tag`,
        `${tables.FORUM}.id`,
        `${tables.FORUM}.created_at`,
        `${tables.FORUM}.image`,
        `${tables.USERS}.username`,
        `${tables.USERS}.display_name`,
        `${tables.USERS}.picture`,
      ).join(tables.USERS, `${tables.USERS}.id`, `=` , `${tables.FORUM}.user_id`)
      .join(tables.CATEGORIES, `${tables.CATEGORIES}.id`, `=` , `${tables.FORUM}.category_id`)
      .where(`${tables.FORUM}.id`, String(id))
      .first()
    return data;
  };

  getAllDiscussion = async (user_id:string) => {
    const data = await this.knex(tables.FORUM)
    .select(`${tables.FORUM}.id AS discuss_id`, 
    `${tables.FORUM}.title`, 
    `${tables.CATEGORIES}.category`,
    `${tables.FORUM}.description`,
    `${tables.FORUM}.hash_tag`,
    `${tables.FORUM}.created_at`,
    `${tables.USERS}.id AS user_id`,
    `${tables.USERS}.display_name`,
    `${tables.USERS}.username`,
    `${tables.USERS}.picture`,
    `${tables.FORUM}.image`)
    .join(tables.USERS, `${tables.USERS}.id`, `=` , `${tables.FORUM}.user_id`)
    .join(tables.CATEGORIES, `${tables.CATEGORIES}.id`, `=` , `${tables.FORUM}.category_id`)

    .orderBy(`${tables.FORUM}.created_at`,'desc')
    return data;
  };

  getSingleDiscussion = async (comment_id :string) => {
    const data = await this.knex(tables.FORUM)
    .select(`${tables.FORUM}.id AS discuss_id`, 
    `${tables.FORUM}.title`, 
    `${tables.CATEGORIES}.category`,
    `${tables.FORUM}.description`,
    `${tables.FORUM}.hash_tag`,
    `${tables.FORUM}.created_at`,
    `${tables.USERS}.id AS user_id`,
    `${tables.USERS}.display_name`,
    `${tables.USERS}.username`,
    `${tables.USERS}.picture`,
    `${tables.FORUM}.image`)
    .join(tables.USERS, `${tables.USERS}.id`, `=` , `${tables.FORUM}.user_id`)
    .join(tables.CATEGORIES, `${tables.CATEGORIES}.id`, `=` , `${tables.FORUM}.category_id`)
    .orderBy(`${tables.FORUM}.created_at`,'desc')
    .where(`${tables.FORUM}.id`, comment_id)
    return data;
  };


  addSingleComment = async (user_id:number, discuss_id:number, comment:string) => {
    
    await this.knex(tables.COMMENT)
    .insert({
      user_id: user_id,
      discuss_id: discuss_id,
      comment: comment,
    })

    const data = await this.knex(tables.COMMENT)
    .select(`${tables.USERS}.id AS user_id`,
    `${tables.USERS}.display_name`,
    `${tables.USERS}.username`,
    `${tables.USERS}.picture`,
    `${tables.COMMENT}.comment`,
    `${tables.COMMENT}.created_at`)
    .join(tables.USERS, `${tables.USERS}.id`, `=` , `${tables.COMMENT}.user_id`)
    .where(`${tables.COMMENT}.discuss_id`, String(discuss_id))
    .orderBy(`${tables.COMMENT}.created_at`, `asc`)
    console.log("this is data 1"+ data)
    
    return data;
  };
  getAllComment = async (id: string) => {
    
    const data = await this.knex(tables.COMMENT)
    .select(`${tables.USERS}.id AS user_id`,
    `${tables.USERS}.display_name`,
    `${tables.USERS}.username`,
    `${tables.USERS}.picture`,
    `${tables.COMMENT}.comment`,
    `${tables.COMMENT}.created_at`)
    .join(tables.USERS, `${tables.USERS}.id`, `=` , `${tables.COMMENT}.user_id`)
    .where(`${tables.COMMENT}.discuss_id`, id)
    .orderBy(`${tables.COMMENT}.created_at`, `asc`)
    console.log("this is data "+ data)
    return data
  };

  getAllForumCommentLength = async (discuss_id: string) => {
    const data = await this.knex(tables.COMMENT).where(`${tables.COMMENT}.discuss_id`, discuss_id)
    return data
}
  getAllDiscussionByUserID = async (user_id: string) => {
    const data = await this.knex(tables.FORUM)
    .select(`${tables.FORUM}.id AS discuss_id`, 
    `${tables.FORUM}.title`, 
    `${tables.CATEGORIES}.category`,
    `${tables.FORUM}.description`,
    `${tables.FORUM}.hash_tag`,
    `${tables.FORUM}.created_at`,
    `${tables.FORUM}.image`,
    `${tables.USERS}.id AS user_id`,
    `${tables.USERS}.display_name`,
    `${tables.USERS}.username`,
    `${tables.USERS}.picture`)
    .join(tables.USERS, `${tables.USERS}.id`, `=` , `${tables.FORUM}.user_id`)
    .join(tables.CATEGORIES, `${tables.CATEGORIES}.id`, `=` , `${tables.FORUM}.category_id`)
    .orderBy(`${tables.FORUM}.created_at`,'desc')
    .where(`${tables.USERS}.id`, user_id)

    return data;
  }

  removeForumDiscussion = async (discuss_id: string) => {

    // await this.knex(tables.COMMENT)
    // .where(`${tables.COMMENT}.discuss_id`, discuss_id);

    // 1. 要transaction
    // 2, mark delete
    await this.knex(tables.FAV)
    .where(`${tables.FAV}.discuss_id`, discuss_id)
    .del()
    

    await this.knex(tables.COMMENT)
    .where(`${tables.COMMENT}.discuss_id`, discuss_id)
    .del()
    
    await this.knex(tables.FORUM)
    .where(`${tables.FORUM}.id`, discuss_id)
    .del()
    // await this.knex(tables.FORUM)
    // .join(tables.COMMENT, `${tables.COMMENT}.discuss_id`, `=` , `${tables.FORUM}.id`)
    // .join(tables.FAV, `${tables.FAV}.discuss_id`, `=` , `${tables.FORUM}.id`)
    // .where(`${tables.FORUM}.id`, discuss_id)
    // .del()
    
  }
  // async loadItemRooms(itemId: number): Promise<ChatUser[]> {
  //   const rooms = await this.knex("rooms").where('item_id', itemId).select('id')
  //   let list = []
  //   for (const room of rooms) {
  //       const detail = await this.knex("rooms").where('room_id', room.id)
  //           .join('users', 'rooms.user_id', 'users.id')
  //           .join('messages', 'rooms.id', 'messages.room_id')
  //           .select('messages.room_id', 'rooms.host_id', 'rooms.item_id', 'rooms.user_id','users.display_name', 'users.picture', 'messages.message', 'messages.created_at')
  //           .orderBy('messages.created_at', 'desc').first()

  //       const unreadMsg = await this.knex('messages').where('room_id', room.id)
  //           .andWhere('is_read', 'false').andWhereNot('sender_id', detail.host_id)

  //       detail.unread = unreadMsg.length
  //       list.push(detail)

  //   }
  //   return list
};
