export const tables = Object.freeze({
    USERS: "users",
    SOCIAL_PLATFORMS: "social_platforms",
    CATEGORIES: "categories",
    FORUM: "forum",
    ITEMS: "items",
    COMMENT: "comment",
    UNREAD:'receiver_id',
    FAV: 'favor',
    IMG: 'image'
});
