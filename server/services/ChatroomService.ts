import Knex from 'knex';
import { ChatUser, Msgs, Rooms } from './models';

interface ChatHost {
    room_id: number;
    user_id: number;
    display_name: string;
    picture: string;
    message: string;
    sendTime: Date;
}

export class ChatroomService {
    constructor(private knex: Knex) { }

    async createChatroom(userId: number, itemId: number): Promise<number[]> {
        // Need database transactions
        return this.knex.transaction(async (trx)=>{

            const check = await trx("rooms")
                .where('user_id', userId).andWhere('item_id', itemId)
                .select('id');
            if (check[0]) {
                return check[0].id
            } else {
                const result = await trx('items').select('creater_id').where('id', itemId);
                const roomId = await trx('rooms').insert({
                    host_id: result[0].creater_id,
                    user_id: userId,
                    item_id: itemId,
                }).returning('id')
                return roomId[0]
            };
        });
    }

    /*
        select rooms.id,max(messages.created_at) from users inner join rooms 
        on users.id = rooms.user_id inner join messages on rooms.id = messages.room_id  where users.id = 1  group by rooms.id;
     */

    /*
        select rooms.id,messages.message from messages where created_at in ('2020-10-15 14:38:48.929925+08');
     */

    async loadUserRooms(userId: number): Promise<ChatHost[]> {
        const rooms = await this.knex("rooms").where('user_id', userId).select('id')
        let list = []
        for (const room of rooms) {
            // N + 1 Problem
            const detail = await this.knex("rooms").where('rooms.id', room.id)
                .innerJoin('users', 'rooms.host_id', 'users.id')
                .innerJoin('messages', 'rooms.id', 'messages.room_id')
                .select('rooms.id', 'rooms.user_id', 'rooms.item_id', 'rooms.host_id', 'users.display_name', 'users.picture', 'messages.message', 'messages.created_at')
                .orderBy('messages.created_at', 'desc').first()
            if (detail) {
                list.push(detail)
            } else {
                const noMsg = await this.knex("rooms").where('rooms.id', room.id)
                    .join('users', 'rooms.host_id', 'users.id')
                    .select('rooms.id', 'rooms.host_id', 'rooms.item_id', 'rooms.user_id', 'users.display_name', 'users.picture')
                    .first()
                list.push(noMsg)
            }
        }
        return list
    }

    async loadHostItems(userId: number): Promise<Rooms[]> {
        const items = await this.knex("items").where('creater_id', userId)
        return items
    }

    async loadItemRooms(itemId: number): Promise<ChatUser[]> {
        const rooms = await this.knex("rooms").where('item_id', itemId).select('id')
        let list = []
        for (const room of rooms) {
            const detail = await this.knex("rooms").where('room_id', room.id)
                .join('users', 'rooms.user_id', 'users.id')
                .join('messages', 'rooms.id', 'messages.room_id')
                .select('messages.room_id', 'rooms.host_id', 'rooms.item_id', 'rooms.user_id', 'users.display_name', 'users.picture', 'messages.message', 'messages.created_at')
                .orderBy('messages.created_at', 'desc').first()
            if (detail) {
                list.push(detail)
            };
        }
        return list
    }

    async loadAllMsgs(userId: number): Promise<Msgs[]> {
        const msgs = await this.knex('messages').orderBy('id')
            .where('sender_id', userId).orWhere('receiver_id', userId)
        return msgs
    }

    async addMsg(userId: number, roomId: number, msg: string, rcvrId: number,) {

        const msgId = await this.knex('messages').insert({
            room_id: roomId,
            sender_id: userId,
            receiver_id: rcvrId,
            message: msg,
            is_read: false
        }).returning('id')
        const msgContent = await this.knex('messages').where('id', msgId[0])
        return msgContent

    }

    async readMsg(roomId: number, userId: number) {
        // No need transaction, Becoz only one SQL
        const readMsg = await this.knex('messages')
            .where('room_id', roomId).andWhere('is_read', 'false').andWhere('receiver_id', userId)
            .update({ is_read: true }).returning('*')
        return readMsg
    }
}