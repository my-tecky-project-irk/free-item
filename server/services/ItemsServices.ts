import Knex from "knex";
import { tables } from "./tables";
import moment from 'moment';

export class ItemsService {
    constructor(private knex: Knex) { }

    getAllItems = async () => {
        const itemsId = await this.knex(tables.ITEMS).select('id')
            .where('expire_day', '>', moment())
            .where('is_available', 'true')
            .orderBy(`${tables.ITEMS}.created_at`, "desc");
        let list = [];
        for (const id of itemsId) {
            const items = await this.knex(tables.ITEMS).where('items.id', id.id)
                .select(
                    `${tables.ITEMS}.id`,
                    `${tables.ITEMS}.creater_id`,
                    `${tables.ITEMS}.title`,
                    'image.path',
                    //  `${tables.ITEMS}.intro`,
                    //  `${tables.ITEMS}.pickup_times`,
                    `items.category_id`,
                    // `${tables.ITEMS}.expire_day`,
                    `${tables.ITEMS}.is_available`,
                    // `${tables.ITEMS}.created_at`,
                    // new add by Karl
                    `${tables.USERS}.display_name`,
                    // `${tables.USERS}.username`,
                    `${tables.USERS}.picture`)
                .join('users', `items.creater_id`, `users.id`)
                .join('categories', `items.category_id`, `categories.id`)
                .join('image', 'items.id', 'image.item_id').first();
            if (items) {
                list.push(items)
            } else {
                const noImg = await this.knex(tables.ITEMS).where('items.id', id.id)
                    .select(
                        `${tables.ITEMS}.id`,
                        `${tables.ITEMS}.creater_id`,
                        `${tables.ITEMS}.title`,
                        `items.category_id`,
                        `${tables.USERS}.display_name`,
                        `${tables.USERS}.picture`)
                    .join('users', `items.creater_id`, `users.id`)
                    .join('categories', `items.category_id`, `categories.id`).first()
                list.push(noImg)
            }
        }
        //console.log(list)
        return list;
    };

    // 包在一個object
    createItem = async (
        userID: number,
        title: string,
        intro: string,
        pickup_times: string,
        categoryID: number,

        listing_days: number,
        lat: number,
        lng: number,
        imgs: string[],

    ) => {
        const id = await this.knex(tables.ITEMS)
            .insert({
                creater_id: userID,
                title: title,
                intro: intro,
                pickup_times: pickup_times,
                category_id: categoryID,
                listing_days: listing_days,
                expire_day: moment().add(listing_days, 'days'),
                is_available: true,
                lat: lat,
                lng: lng,
            }).returning("*");

        if (imgs[0]) {

            // await this.knex('image').insert(imgs.map(img=>({path:img,item_id:id[0].id})));
            for (const img of imgs) {
                await this.knex('image').insert({ item_id: id[0].id, path: img })
            }
        }
        return id;
    };

    getItemInfo = async (item_id: string) => {
        const itemInfo = await this.knex(tables.ITEMS).where(`items.id`, item_id)
            .join(tables.CATEGORIES, `items.category_id`, `categories.id`)
            .join('users', `items.creater_id`, `users.id`)
            .select(
                `${tables.ITEMS}.id`,
                `${tables.ITEMS}.title`,
                `${tables.ITEMS}.intro`,
                `${tables.ITEMS}.pickup_times`,
                `${tables.ITEMS}.expire_day`,
                `${tables.ITEMS}.is_available`,
                `${tables.ITEMS}.created_at`,
                `${tables.ITEMS}.lat`,
                `${tables.ITEMS}.lng`,
                `${tables.CATEGORIES}.category`,
                `${tables.USERS}.display_name`,
                `${tables.USERS}.picture`,
                `${tables.USERS}.id AS user_id`,
                'items.category_id',
            ).first();
        return itemInfo;
    };

    getAllItemByUserID = async (user_id: string) => {
        //userID: number
        const item = await this.knex(tables.ITEMS)
            .select(
                `${tables.ITEMS}.id`,
                `${tables.ITEMS}.creater_id`,
                `${tables.ITEMS}.title`,
                `${tables.ITEMS}.intro`,
                `${tables.ITEMS}.pickup_times`,
                `${tables.CATEGORIES}.category`,
                `${tables.ITEMS}.expire_day`,
                `${tables.ITEMS}.is_available`,
                `${tables.ITEMS}.created_at`,
                // new add by Karl
                `${tables.USERS}.display_name`,
                `${tables.USERS}.username`,
                `${tables.USERS}.picture`,
            )
            .join(tables.CATEGORIES, `items.category_id`, `categories.id`)
            .join(tables.USERS, `items.creater_id`, `users.id`)
            .where(`${tables.USERS}.id`, user_id)
            .orderBy(`${tables.ITEMS}.created_at`, "desc");
        return item;
    };


    getImage = async (itemId: string) => {
        const imgs = await this.knex('image').where('item_id', itemId)
        return imgs

    }

    removeSingleItem = async (item_id: string) => {

        // Room can be marked delete
        await this.knex('rooms').where('item_id',item_id).del();

        await this.knex(tables.IMG)
        .where(`${tables.IMG}.item_id`, item_id)
        // .join(tables.ITEMS, `${tables.IMG}.items.id`, `=` , `${tables.ITEMS}.id`)
        .del()

        const item_title = await this.knex(tables.ITEMS)
        .where(`id`, item_id)
        .returning("title")
        .del()

        
        // await this.knex('messages').where('room_id',roomId).del();
        
        
        
        
        return item_title;
    }
    // getAllLabels = async () => {
    //     return await this.knex(tables.CATEGORIES);
    // };

    arrangePickup = async (itemId: string, state: boolean) => {
        const item = await this.knex('items').where('id', itemId)
            .update('is_available', `${state}`).returning('*')
        return item


    }
}
