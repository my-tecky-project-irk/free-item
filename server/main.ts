import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import dotenv from "dotenv";
import socketIO from "socket.io";
import http from "http";
import multer from "multer";
import multerS3 from 'multer-s3';
import aws from 'aws-sdk'; //食m 'aws-sdk'; //食咗幾百MB ram

// const storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         cb(null, `${__dirname}/public/uploads`);
//     },
//     filename: function (req, file, cb) {
//         cb(
//             null,
//             `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
//         );
//     },
// });
// export const upload = multer({ storage });

// dropbox 咁嘅地方
const s3 = new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: "ap-southeast-1",
});

export const upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: "your-bucket-name",
        metadata: (req, file, cb) => {
            cb(null, { fieldName: file.fieldname });
        },
        key: (req, file, cb) => {
            cb(
                null,
                `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
            );
        },
    }),
});

dotenv.config();
const app = express();
const server = http.createServer(app);
const io = socketIO(server);

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

import Knex from "knex";
const knexConfig = require("./knexfile");
const knex = Knex(knexConfig["development"]);

// 個import 同個usage 好近，咁好易搵得到
import { UserService } from "./services/UserService";
import { UserController } from "./controllers/UserController";
const userService = new UserService(knex);
export const userController = new UserController(userService);

import { ForumService } from "./services/ForumService";
import { ForumController } from "./controllers/ForumController";
const forumService = new ForumService(knex);
export const forumController = new ForumController(forumService);

import { ChatroomService } from "./services/ChatroomService";
import { ChatroomController } from "./controllers/ChatroomController";
const chatroomService = new ChatroomService(knex);
export const chatroomController = new ChatroomController(chatroomService, io);

import { ItemsService } from "./services/ItemsServices";
import { ItemsController } from "./controllers/ItemsController";
const itemsService = new ItemsService(knex);
export const itemsController = new ItemsController(itemsService);

import { CategoriesService } from "./services/CategoriesServices";
import { CategoriesController } from "./controllers/CategoriesController";
const categoriesService = new CategoriesService(knex);
export const categoriesController = new CategoriesController(categoriesService);

import { createIsLoggedIn } from "./guards";
export const isLoggedIn = createIsLoggedIn(userService);

import { routes } from "./routes";

// 如果你曾經出過v1，咁你v1基本上要support 好耐until 無client 用v1 為止
// 出breaking changes，果個更動係會令原本嘅client 炒
const API_VERSION = "/api/v1";
app.use(API_VERSION, routes);

app.use(express.static("public"));

io.on("connection", (socket) => {
    socket.on("join_room", (roomsId: number) => {
        socket.join("room:" + roomsId);
    });
    socket.on("leave_room", (roomsId: number) => {
        socket.leave("room:" + roomsId);
    });
});

const PORT = 8080;
server.listen(PORT, () => {
    console.log(`connected to ${PORT} port`);
});
