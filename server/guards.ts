import {Bearer} from 'permit';
import jwtSimple from 'jwt-simple';
import express from 'express';
import jwt from './jwt';
import {UserService} from './services/UserService';

const permit = new Bearer({
  query: 'access_token',
})

export function createIsLoggedIn(userService: UserService){
  return async function(req: express.Request , res: express.Response, next: express.NextFunction){
    try{
   
      const token = permit.check(req)
      if (!token) {
        return res.status(401).json({msg: 'Permission Denied'})
      }
      const payload = jwtSimple.decode(token, jwt.jwtSecret);
      const user = await userService.getUserByUserID(payload.id);
      if(user) {
     
        const {password, ...others} = user;
        req.user = { ... others};
        
        return next();
      } else {
            return res.status(401).json({msg: 'Permission Denied'})
      }
    }catch (e) {
      return res.status(401).json({msg: 'Permission Denied'})
    }
  }
}