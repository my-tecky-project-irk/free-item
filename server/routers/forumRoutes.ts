import express from 'express';
import { forumController, upload } from '../main';

export const forumRoutes = express.Router();

forumRoutes.get('/discussByUserID/:user_id', forumController.getAllDiscussionByUserID)
forumRoutes.post('/discuss', upload.single("image"), forumController.addSingleDiscussion);
// forumRoutes.post('/discuss', forumController.addSingleDiscussion);
forumRoutes.get('/content/', forumController.getAllDiscussion);
// forumRoutes.get('/content/:user_id', forumController.getAllDiscussionWithFavByID);
forumRoutes.get('/discuss/:discuss_id', forumController.getSingleDiscussion);
forumRoutes.post('/singlecomment', forumController.addSingleComment);
forumRoutes.get('/allcomment/:discuss_id', forumController.getAllComment);
forumRoutes.get('/content/commentlength/:discuss_id', forumController.getAllForumCommentLength)
forumRoutes.delete('/removediscussion/:discuss_id', forumController.removeForumDiscussion);
