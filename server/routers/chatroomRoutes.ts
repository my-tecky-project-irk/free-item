import express from 'express';
import { chatroomController, isLoggedIn } from '../main';


export const chatroomRoutes = express.Router();

chatroomRoutes.post('/createchatroom/:id', isLoggedIn,chatroomController.createChatroom);
chatroomRoutes.get('/items', isLoggedIn,chatroomController.loadHostItems);
chatroomRoutes.get('/itemrooms/:id', isLoggedIn,chatroomController.loadItemRooms);
chatroomRoutes.get('/userrooms', isLoggedIn,chatroomController.loadUserRooms)
chatroomRoutes.get('/msgs',isLoggedIn, chatroomController.loadAllMsgs);
chatroomRoutes.post("/send",isLoggedIn, chatroomController.sendMsg);
chatroomRoutes.put("/readmsg/:id", isLoggedIn,chatroomController.readMsg);

