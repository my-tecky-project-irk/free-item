import * as Knex from "knex";

const userTable = 'users'
const forumTable = 'forum'
const favorTable = 'favor'


export async function up(knex: Knex): Promise<void> {

  await knex.schema.createTable(favorTable, (table)=>{
      table.increments();
      table.integer('user_id').unsigned().notNullable();
      table.foreign("user_id").references(`${userTable}.id`)
      table.integer('discuss_id').unsigned().notNullable();
      table.foreign("discuss_id").references(`${forumTable}.id`)
      table.boolean('isforumliked')
      table.timestamps(false, true);
  })
}




export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(favorTable);

}
