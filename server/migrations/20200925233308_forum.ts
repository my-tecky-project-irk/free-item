import * as Knex from "knex";

const forumTable = "forum";
const userTable = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(forumTable, (table) => {
        table.increments();
        table.integer("user_id").unsigned().notNullable();
        table.foreign("user_id").references(`${userTable}.id`);
        table.string("title").notNullable();
        table.integer("category_id").unsigned().notNullable();
        table.foreign("category_id").references("categories.id");
        table.string("description", 1000).notNullable();
        table.jsonb("hash_tag").notNullable();
        // mark delete
        // table.boolean('active').defaultTo(true);
        table.string("image");
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(forumTable);
}
