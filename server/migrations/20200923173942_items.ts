import * as Knex from "knex";
const itemsTable = "items";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(itemsTable, (table) => {
        table.increments();
        table.integer("creater_id").unsigned().notNullable();
        table.foreign("creater_id").references("users.id");
        table.integer("category_id").unsigned().notNullable();
        table.foreign("category_id").references("categories.id");
        table.string("title").notNullable();
        table.string("intro").notNullable();
        table.date("expire_day").notNullable();    
        table.string("pickup_times");
        table.integer("listing_days");
        table.boolean("is_available").notNullable();
        table.string("lat");
        table.string("lng");
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(itemsTable);
}
