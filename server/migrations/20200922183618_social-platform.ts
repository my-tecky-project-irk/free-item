import * as Knex from "knex";

const socialPlatformTable = 'social_platforms'
const usersTable = 'users'

export async function up(knex: Knex): Promise<void> {

  await knex.schema.createTable(socialPlatformTable, (table)=> {
    table.increments();
    table.integer('user_id').unsigned().notNullable();
    table.foreign('user_id').references(`${usersTable}.id`)
    table.string('platform_type').notNullable();
    table.string('platform_id').notNullable().unique();
    table.timestamps(false, true);    
  })
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(socialPlatformTable);
}

