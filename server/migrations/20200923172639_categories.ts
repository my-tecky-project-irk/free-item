import * as Knex from "knex";
const catsTable = "categories";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(catsTable, (table) => {
        table.increments();
        table.string("category").notNullable();
        table.boolean("is_forum").notNullable();
        table.timestamps(false, true);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(catsTable);
}
