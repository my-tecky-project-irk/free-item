import * as Knex from "knex";
const msgsTable = 'messages'

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(msgsTable, (table) => {
        table.increments();
        table.integer('room_id').unsigned().notNullable();
        table.foreign('room_id').references('rooms.id');
        table.integer('sender_id').notNullable();
        table.foreign('sender_id').references('users.id');
        table.integer('receiver_id').notNullable();
        table.foreign('receiver_id').references('users.id');
        table.string('message').notNullable();
        table.boolean('is_read').notNullable();
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(msgsTable);
}
