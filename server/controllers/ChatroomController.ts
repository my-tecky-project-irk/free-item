
import express from "express";
import HttpStatus from "http-status-codes";
import SocketIO from 'socket.io';
import { ChatroomService } from "../services/ChatroomService";

export class ChatroomController {
    constructor(private chatroomService: ChatroomService, private io: SocketIO.Server) { };

    createChatroom = async (req: express.Request, res: express.Response) => {
        try {
            if (req.user) {
                // const user = req.user as User;
                const userId = req.user.id;
                const itemId = parseInt(req.params.id);              
                const result = await this.chatroomService.createChatroom(userId, itemId);
                return res.json({ success: true, roomId: result });
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Login Please." });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "create chatroom : internal server error" });
        }
    };

    loadHostItems = async (req: express.Request, res: express.Response) => {
        try {
            if (req.user) {
                const userId = req.user.id;
                const items = await this.chatroomService.loadHostItems(userId)
                return res.json(items);
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Please Login!" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "load host item :internal server error" });
        }
    };

    loadItemRooms = async (req: express.Request, res: express.Response) => {
        try {
            if (req.user) {
                const itemId = parseInt(req.params.id)
                const rooms = await this.chatroomService.loadItemRooms(itemId)
                return res.json(rooms);
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Please Login!" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "load item rooms :internal server error" });
        }
    };

    loadUserRooms = async (req: express.Request, res: express.Response) => {
        try {
            if (req.user) {
                const userId = req.user.id;                
                const rooms = await this.chatroomService.loadUserRooms(userId)
                return res.json(rooms);
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Please Login!" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "load user rooms :internal server error" });
        }
    };

    loadAllMsgs = async (req: express.Request, res: express.Response) => {
        try {
            if (req.user) {
                const userId = req.user.id;
                const msgs = await this.chatroomService.loadAllMsgs(userId)
                return res.json(msgs);
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Please Login!" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "load all msg : internal server error" });
        }
    };


    sendMsg = async (req: express.Request, res: express.Response) => {
        try {
            if (req.user) {
                const userId = req.user.id;
                const roomId = parseInt(req.body.roomId);
                const msg = req.body.msgContent;
                const rcvId = req.body.rcvId
                const result = await this.chatroomService.addMsg(userId, roomId, msg, rcvId);
                console.log(result[0])
                this.io.to('room:' + roomId).emit('new_msg', result[0]);
                return res.json({ success: true, msgId: result });
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Login Please." });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "send msg : internal server error" });
        }
    };

    readMsg = async (req: express.Request, res: express.Response) => {
        try {
            if (req.user) {
                const roomId = parseInt(req.params.id);
                const userId = req.user.id;
                const readmsg = await this.chatroomService.readMsg(roomId, userId)

                this.io.to('room:' + roomId).emit('read_msg', readmsg);
                return res.json({ success: true });
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Please Login!" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "read msg : internal server error" });
        }
    };

}
