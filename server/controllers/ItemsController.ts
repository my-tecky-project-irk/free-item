import { Request, Response } from "express";
import { ItemsService } from "../services/ItemsServices";


export class ItemsController {
    constructor(private itemsService: ItemsService) { }
    getAllItems = async (req: Request, res: Response) => {
        try {
            const item = await this.itemsService.getAllItems();
            res.json(item);
            return
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "get all items : internal server error" });
            return
        }
    };

    createItem = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                // validation is difficult anyway
                const userId = req.user.id;
                const title = req.body.title;
                const intro = req.body.intro;
                const pickup_times = req.body.pickup_times;
                const category_id = req.body.category_id;
                const listing_days = req.body.listing_days;
                const lat = req.body.lat;
                const lng = req.body.lng;
                const image = req.files['image'];
                let imgs: string[] = [];
                if (image) {
                    for (let i = 0; i < image.length; i++) {
                        imgs.push(image[i].filename)
                    }
                }
                const id = await this.itemsService.createItem(
                    userId,
                    title,
                    intro,
                    pickup_times,
                    category_id,
                    listing_days,
                    lat,
                    lng,
                    imgs,
                );
                res.json(id);
                return
            }
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "create item : internal server error" });
            return
        }
    };

    getItemInfo = async (req: Request, res: Response) => {
        try {
            // console.log(req.params.item_id);
            const itemInfo = await this.itemsService.getItemInfo(req.params.item_id);
            res.json(itemInfo);
            return
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: " get item info : internal server error" });
            return
        }
    };

    getAllCategoriesItemByUserID = async (req: Request, res: Response) => {
        try {
            // const userID = req.user?.id;
            // let categories;
            // if (userID) {
            // console.log(userID);
            const item = await this.itemsService.getAllItemByUserID(req.params.user_id);
            // }
            //  console.log(item);
            res.json(item);
            return
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "internal server error" });
            return
        }
    };

    getImage = async (req: Request, res: Response) => {
        try {
            const imgs = await this.itemsService.getImage(req.params.item_id);
            return res.json(imgs);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: " get item info : internal server error" });
            return
        }
    };
    removeSingleItem = async (req: Request, res: Response) => {
        try {
            // console.log(req.params.item_id);
            const item_title = await this.itemsService.removeSingleItem(req.params.item_id);
            res.json({item_title});
            return
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: " get item info : internal server error" });
        }
    }

    arrangePickup = async (req: Request, res: Response) => {
        try {
            if (req.user) {
                const itemId = req.params.id
                const state = req.body.is_available
                const pickup = await this.itemsService.arrangePickup(itemId, state)
                return res.json( pickup[0] );
            }
            return res.status(500).json({ success: false, message: "Please Login!" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "read msg : internal server error" });
        }
    }
}
