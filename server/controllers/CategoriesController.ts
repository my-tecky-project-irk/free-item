
import { Request, Response } from "express";
import { CategoriesService } from "../services/CategoriesServices";

export class CategoriesController {
    constructor(private categoriesService: CategoriesService) { }

    // Not used
    getForumCats= async (req: Request, res: Response) => {
        try {
            const cats= await this.categoriesService.getForumCats();
            res.json(cats);
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "get forum cats : internal server error" });
        }
    };

  
    getItemCats= async (req: Request, res: Response) => {
        try {
            const cats= await this.categoriesService.getItemCats();
            res.json(cats);
        } catch (err) {
            console.error(err.message);
            // can use error code
            res.status(500).json({ message: "Categories001 get item cats : internal server error" });
        }
    };

}
