import { authfetch } from "../common";
import { ThunkDispatch } from "../store";
import { forumCats, itemCats } from "./actions";


export function thunkForumCats() {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/categories/forum`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        const json = await res.json();
        dispatch(forumCats(json,'forum'))
    }
}

export function thunkItemCats() {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/categories/item`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        // if(res.status === 500){
        //     dispatch();// dispatch show toast action
        // };

        // const data = await authfetch(`${process.env.REACT_APP_API_SERVER}/categories/item`);
        const json = await res.json();    
     //   console.log(json)   
        dispatch(itemCats(json,'item'))
    }
}


// import { Dispatch } from "redux";
// import { loginFail } from "../auth/actions";
// import {
//     addCategoriesFail,
//     addCategoriesSucess,
//     setCategoriesItems,
//     setIsPorcessing,
// } from "./actions";

// const { REACT_APP_API_SERVER } = process.env;

// export const getAllCategoriesThunk = () => {
//     return async (dispatch: Dispatch) => {
//         const token = localStorage.getItem("token");
//         if (!token) {
//             dispatch(loginFail(""));
//         }

//         const res = await fetch(`${REACT_APP_API_SERVER}/categories/items`, {
//             headers: {
//                 Authorization: `Bearer ${token}`,
//             },
//         });
//         if (res.status === 401) {
//             dispatch(loginFail(""));
//         }
//         if (res.status === 200) {
//             const result = await res.json();

//             dispatch(setCategoriesItems(result.item));
//         }
//     };
// };

// export const createCategoriesItemThunk = (
//     title: string,
//     description: string,
//     labelValue: number,
//     image: any,
//     expireDay: string,
//     lat: any,
//     lng: any
// ) => {
//     return async (dispatch: Dispatch) => {
//         const token = localStorage.getItem("token");
//         if (!token) {
//             dispatch(loginFail(""));
//         }
//         dispatch(setIsPorcessing());
//         const formData = new FormData();
//         formData.append("title", title);
//         formData.append("intro", description);
//         formData.append("category_id", labelValue.toString());
//         formData.append("image", image);
//         formData.append("expire_day", expireDay);
//         formData.append("lat", lat);
//         formData.append("lng", lng);

//         const res = await fetch(`${REACT_APP_API_SERVER}/categories/submit`, {
//             method: "POST",

//             headers: {
//                 Authorization: `Bearer ${token}`,
//                 // "Content-type": "application/json",
//             },
//             body: formData,
//             // body: JSON.stringify({
//             //     title: title,
//             //     intro: description,
//             //     category_id: labelValue,
//             //     image: image,
//             // }),
//         });

//         if (res.status === 401) {
//             dispatch(loginFail(""));
//         }
//         if (res.status === 200) {
//             const data = await res.json();
//             const item = {
//                 id: data.id,
//                 category_id: labelValue,
//                 title: title,
//                 intro: description,
//                 expire_day: expireDay,
//                 is_available: true,
//                 image: image,
//                 lat: lat,
//                 lng: lng,
//             };
//             dispatch(addCategoriesSucess(item));
//         } else {
//             dispatch(addCategoriesFail());
//         }
//     };
// };


// export const getAllCategoriesByUserIDThunk = (user_id: string) => {
//     return async (dispatch: Dispatch) => {
//         const token = localStorage.getItem("token");
//         if (!token) {
//             dispatch(loginFail(""));
//         }

//         const res = await fetch(`${REACT_APP_API_SERVER}/categories/items/user/${user_id}`, {
//             headers: {
//                 Authorization: `Bearer ${token}`,
//             },
//         });
//         if (res.status === 401) {
//             dispatch(loginFail(""));
//         }
//         if (res.status === 200) {
//             const result = await res.json();

//             dispatch(setCategoriesItems(result.item));
//         }
//     };
// };