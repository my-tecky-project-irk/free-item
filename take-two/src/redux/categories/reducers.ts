
import produce from 'immer'
import { ICategoriesActions } from './actions';
import {  ICategoriesState, initialState } from './state';



export function categoriesReducers(state:ICategoriesState  = initialState, action:  ICategoriesActions): ICategoriesState   {
    return produce(state, state => {
        switch (action.type) {

            case '@@categories/FORUM': 
                if (action.forumCats !== null) {
                    for (const cat of action.forumCats) {
                        state.catsById[cat.id] = cat                      
                    }
                    state.catsByItemForum[action.forum]=action.forumCats.map(cats=>cats.id)
                }
             break;

            case '@@categories/ITEM': 
                if (action.itemCats !== null) {
                    for (const cat of action.itemCats) {
                        state.catsById[cat.id] = cat                      
                    }
                    state.catsByItemForum[action.item]=action.itemCats.map(cats=>cats.id)
                }
             break;
        }
    })


}




// import { ICategoriesState, initCategoriesState } from "./state";
// import { ICategoriesActions } from "./actions";
// import {
//     SET_CATEGORIES_ITEMS,
//     SET_IS_PROCESSING,
//     ADD_CATEGORIES_SUCCESS,
//     ADD_CATEGORIES_FAIl,
// } from "./actions";

// export const categoriesReducers = (
//     state: ICategoriesState = initCategoriesState,
//     action: ICategoriesActions
// ): ICategoriesState => {
//     switch (action.type) {
//         case SET_IS_PROCESSING:
//             return {
//                 ...state,
//                 isProcessing: true,
//             };
//         case ADD_CATEGORIES_SUCCESS:
//             return {
//                 ...state,
//                 isProcessing: false,
//             };
//         case ADD_CATEGORIES_FAIl:
//             return {
//                 ...state,
//                 isProcessing: false,
//             };
//         case SET_CATEGORIES_ITEMS:
//             return {
//                 ...state,
//                 categoriesItems: action.categoriesItems,
//             };
//         default:
//             return state;
//     }
// };
