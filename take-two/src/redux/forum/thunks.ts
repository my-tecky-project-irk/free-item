import { Dispatch } from "redux";

import {IuserInfo} from '../auth/state'
import { addDiscussSuccess, setAllDiscussion, addSingleComment, setAllComment, discussProcessing } from './actions';
import {IFormInputs} from "../../pages/ForumPage"
import {Icomment} from '../../components/forum/ForumChat'
import {IdiscussInfo} from './state'
import { IRootState } from "../store";

const { REACT_APP_API_SERVER } = process.env;

export const addSingleDiscussionThunk=(userInfo: IuserInfo, data: IFormInputs)=> {
  return async (dispatch: Dispatch) => {
  //  console.log(data)
    dispatch(discussProcessing());
    const formData = new FormData();
    // user_id: number | null;
    // title: string;
    // category: string;
    // description: string;
    // hashtag: string[];
    // image: File | undefined;
    formData.append("user_id", String(data.user_id));
    formData.append("title", data.title);
    formData.append("category_id", data.category);
    formData.append("description", data.description);
    formData.append("hashtag", data.hashtag);
    formData.append("image", data.image[0]);


    const res = await fetch (`${REACT_APP_API_SERVER}/forum/discuss`, {
      method: 'POST',
      headers: {
          // 'Content-Type': 'application/json',
      },
      body: formData,
  });
  
    if (res.status === 200) {
      const data = await res.json();
      console.log(data)
     // console.log(data.data)
      const item ={
        //discussID after fetch
        discuss_id: data.data.id,
        created_at: data.data.created_at,
        title: data.data.title,
        category: data.data.category,
        description: data.data.description,
        hash_tag: data.data.hash_tag,
        //userInfo from redux
        user_id: userInfo.id,
        username: userInfo.email,
        display_name: userInfo.display_name,
        picture: userInfo.picture,
        comments: [],
        data: [],
        liked: false,
        image: data.data.image
        //form submission info
        
      }
    //  console.log(item)
      dispatch(addDiscussSuccess(item))
    }
  }
}

export const getAllDiscussionThunk=(id:number | null)=> {
  return async (dispatch: Dispatch) => {
    const res = await fetch (`${REACT_APP_API_SERVER}/forum/content/`);  
    
    if(res.status === 200 ) {
      const data = await res.json();
      let array: Array<IdiscussInfo> = []
      let join: IdiscussInfo  = {
          discuss_id: 0,
          user_id: null,
          username: "",
          display_name: '',
          picture: '',
          title: '',
          category: '',
          description: '',
          hash_tag: [],
          created_at: "",
          data: [],
          liked: false,
          image: "",
      }
      for (let index in data){
        for(let i = 0; i < data[index].length; i++){
          const res1 = await fetch (`${REACT_APP_API_SERVER}/forum/content/commentlength/${data[index][i].discuss_id}`);
          const data1 = await res1.json();
          // console.log(data[index][0])
          // console.log(data1)
          join = {...data[index][i],
                  ...data1}
          array.push(join)
      }}
      
      // const res2 = await fetch(`${REACT_APP_API_SERVER}/forum/content/${id}`)
      dispatch(setAllDiscussion(array));
    }
  }
}

// export const getSingleDiscussionThunk = (data: Icomment) => {
//     return async (dispatch: Dispatch) => {
//         const res = await fetch(`${REACT_APP_API_SERVER}/forum/comment`, {
//             method: "POST",
//             headers: {
//                 "Content-Type": "application/json",
//             },
//             body: JSON.stringify({ data }),
//         });
//         if (res.status === 200) {
//             const id = await res.json();

//             // dispatch(setContent(data.data));
//         }
//     };
// };

export const addSingleCommentThunk = (data: Icomment) => {
    return async (dispatch: Dispatch) => {
      //  console.log("addSingleComment");
        const res = await fetch(`${REACT_APP_API_SERVER}/forum/singlecomment`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ data }),
        });
        if (res.status === 200) {
            const result = await res.json();
         //   console.log(result);

            dispatch(addSingleComment(result.data));
        }
    };
};

export const getAllCommentThunk = (discuss_id: string) => {
    return async (dispatch: Dispatch) => {
        //console.log("this is thunks");
        const res = await fetch(
            `${REACT_APP_API_SERVER}/forum/allcomment/` + String(discuss_id)
        );
        if (res.status === 200) {
            const result = await res.json();
            console.log(result)
           // console.log(result.data);
            dispatch(setAllComment(result.data));
        }
    };
};


export const getAllDiscussionByUserIDThunk = (user_id: number) => {
  return async (dispatch: Dispatch) => {
    const res = await fetch (
      `${REACT_APP_API_SERVER}/forum/discussByUserID/${user_id}`
    );
    if(res.status === 200 ) {
      const data = await res.json();
      let array: Array<IdiscussInfo> = []
      let join: IdiscussInfo  = {
          discuss_id: 0,
          user_id: null,
          username: "",
          display_name: '',
          picture: '',
          title: '',
          category: '',
          description: '',
          hash_tag: [],
          created_at: "",
          data: [],
          liked: false,
          image: "",
      }
      for (let index in data){
        for(let i = 0; i < data[index].length; i++){
          const res1 = await fetch (`${REACT_APP_API_SERVER}/forum/content/commentlength/${data[index][i].discuss_id}`);
          const data1 = await res1.json();
          // console.log(data[index][0])
          // console.log(data1)
          join = {...data[index][i],
                  ...data1}
          array.push(join)
      }}
      
      // const res2 = await fetch(`${REACT_APP_API_SERVER}/forum/content/${id}`)
      dispatch(setAllDiscussion(array));
    }
  }
}

export const deleteDiscussionThunk = (discuss_id:number) => {
  return async (dispatch:Dispatch<any>, getState: () => IRootState) => {
   await fetch(`${REACT_APP_API_SERVER}/forum/removediscussion/${discuss_id}`, {
      method: 'DELETE'
        })  
        dispatch(getAllDiscussionByUserIDThunk(getState().auth.userInfo.id))
  }
}