import {IdiscussState, initDiscussState} from './state'
import {IdiscussActions} from './actions'
import { ADD_DISUCSS_SUCCESS, ADD_DISUCSS_FAIL, SET_IS_PROCESSING, SET_CONTENT, ADD_SINGLE_COMMENT, SET_ALL_COMMENT} from './actions'

import produce from 'immer';

export const discussReducers = produce((state: IdiscussState, action: IdiscussActions) => {
  switch (action.type) {
    case SET_IS_PROCESSING:
      state.isProcessing = true;
      return;
    case ADD_DISUCSS_SUCCESS:
      state.content.unshift(action.item)
      state.isProcessing = false;
      return;
    case ADD_DISUCSS_FAIL:
      state.isProcessing = false;
      return;
    case SET_CONTENT:
      state.content = action.content;
      return;
    case ADD_SINGLE_COMMENT:
      state.isProcessing = false;
      state.comments = action.comment;
      return;
    case SET_ALL_COMMENT:
      state.isProcessing = false;
      state.comments = action.comment;
      return;
    };
}, initDiscussState)