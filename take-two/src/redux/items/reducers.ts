import produce from 'immer'
import { IItemActions } from './actions';
import { IItemState, initialState } from './state';


export function itemsReducers(state: IItemState = initialState, action: IItemActions): IItemState {
    return produce(state, state => {
        switch (action.type) {

            case '@@items/ALL_ITEMS':

                let i;
                for (i = 0; i < 8; i++) {
                    state.itemByCatId[i] = []
                }
                for (const item of action.items) {

                    //console.log(item.category_id)
                    state.allItemById[item.id] = item
                    state.itemByCatId[7].push(item.id)
                    state.itemByCatId[item.category_id].push(item.id)
                }

                break;

            case '@@items/INFO_ITEM':
                // console.log(action.itemInfo)
                state.itemInfoById[action.itemInfo.id] = action.itemInfo

                break;

            case '@@items/USER_SHARE':
                state.shareItemByPickUp['true'] = [];
                state.shareItemByPickUp['false'] = [];
                state.shareItemByUserId = [];
                for (const share of action.userShares) {
                    state.userShareByItemId[share.id] = share
                    state.shareItemByPickUp[`${share.is_available}`].push(share.id);                    
                }
                state.shareItemByUserId = action.userShares.map(item => item.id);

                break;

            case '@@items/GET_IMAGE':
                for (const img of action.imgs) {
                    state.imgById[img.id] = img
                }
                state.imgByItemId[action.itemId] = action.imgs.map(img => img.id)
                break;

            case '@@items/NEW_ITEM':
                state.allItemById[action.itemInfo.id].creater_id = action.itemInfo.user_id;
                state.allItemById[action.itemInfo.id].display_name = action.itemInfo.display_name;
                state.allItemById[action.itemInfo.id].picture = action.itemInfo.picture;
                state.allItemById[action.itemInfo.id].title = action.itemInfo.title;
                state.allItemById[action.itemInfo.id].id = action.itemInfo.id;
                state.allItemById[action.itemInfo.id].category_id = action.itemInfo.category_id;

                state.itemInfoById[action.itemInfo.id] = action.itemInfo;
                state.itemByCatId[7].push(action.itemInfo.id);
                state.itemByCatId[action.itemInfo.category_id].push(action.itemInfo.id);
                break;

            case '@@items/ARRANGE_PICKUP':
                //state.itemInfoById[action.itemInfo.id].is_available=action.itemInfo.is_available
                state.userShareByItemId[action.itemInfo.id].is_available = action.itemInfo.is_available
               const idx=  state.shareItemByPickUp[`${!action.itemInfo.is_available}`].indexOf(action.itemInfo.id)
               if (idx > -1) {
               state.shareItemByPickUp[`${!action.itemInfo.is_available}`].splice(idx, 1)
              }               
               state.shareItemByPickUp[`${action.itemInfo.is_available}`].push(action.itemInfo.id)
                break
        }

    })


}