export interface ISubmitItem {
    category_id: number;
    title: string;
    intro: string;
    listing_days: number,
    pickup_times: string,
    lat: number;
    lng: number;
    image?: File[];
}

export interface IItemInfo {
    id: number;
    category: string
    title: string;
    intro: string;
    pickup_times: string;
    is_available: boolean;
    expire_day: Date;
    lat: string;
    lng: string;
    created_at: Date;
    display_name: string,
    picture: string,
    user_id: number,
    category_id: number,
}

export interface IAllItems {
    id: number;
    //category: string
    title: string;
    //  intro: string;
    // pickup_times: string;
    // is_available: boolean;
    //  expire_day: Date; 
    //  lat: number;
    //  lng: number;
    //  created_at: Date;
    category_id: number
    display_name: string,
    picture: string,
    creater_id: number,
    image?: string,
}

export interface IImage {
    id: number;
    path: string;
    itemId: number;
}

export interface IUserShare {
    id: number;
    creater_id: number;
    title: string;
    intro: string;
    pickup_times: string;
    category: string;
    expire_day: Date
    is_available: boolean
    created_at: Date

    display_name: string;
    username: string;
    picture: string;
}


export interface IItemState {
    allItemById: {
        [itemId: string]: IAllItems,
    },
    itemByCatId: {
        [catId: string]: number[],
    },


    itemInfoById: {
        [itemId: string]: IItemInfo,
    },

    userShareByItemId: {
        [itemId: string]: IUserShare,
    },
    shareItemByUserId: number[]; 
    shareItemByPickUp: {
        [boolean: string]: number[]
    }

    imgById: {
        [imgId: string]: IImage
    }
    imgByItemId: {
        [ItemId: string]: number[]
    }

}

export const initialState: IItemState = {
    allItemById: {},
    itemByCatId: {},


    itemInfoById: {},

    userShareByItemId: {},
    shareItemByUserId: [],
    shareItemByPickUp: {},
    
    imgById: {},
    imgByItemId: {},
}
















// export interface IAllItemsState {
//     allItems: Array<IItem>;
//     isProcessing: boolean;
// }

// export const initCategoriesState = {
//     allItems: [] as Array<IItem>,
//     isProcessing: false,
// };
export interface ISubmitState {
    items: Array<ISubmitItem>;
    isProcessing: boolean;
}
