export interface IuserInfo {
  id: number;
  display_name: string;
  email: string;
  picture: string;
  lat?: string;
  lng?: string;
  likedForum: number[];
}


export interface IAuthState {
  isAuthenticated: boolean | null;
  isProcessing: boolean;
  errMessage: string;
  userInfo: IuserInfo
}

export const initAuthState: IAuthState = {
  isAuthenticated: null,
  isProcessing: false,
  errMessage: '',
  userInfo: {
    id: 0,
    display_name: "",
    email: '',
    picture: "",
    lat: '',
    lng: '',
    likedForum: [],
  }
}

