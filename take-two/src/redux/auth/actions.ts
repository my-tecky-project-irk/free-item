import { IuserInfo } from './state'


export const loginSuccess = (userInfo: IuserInfo) => {
    return {
        type: '@@auth/LOGIN_SUCCESS' as '@@auth/LOGIN_SUCCESS',
        userInfo,
    }
}
export const loginFail = (message: string) => {
    return {
        type: '@@auth/LOGIN_FAIL' as '@@auth/LOGIN_FAIL',
        message,
    }
}
export const loginIsProcessing = () => {
    return {
        type: '@@auth/LOGIN_IS_PROCESSING' as '@@auth/LOGIN_IS_PROCESSING',
    }
}
export const logoutSuccess = () => {
    return {
        type: '@@auth/LOGOUT_SUCCESS' as '@@auth/LOGOUT_SUCCESS',
    }
}
export const addForumLiked = (likedForum: []) => {
    return {
        type: '@@auth/ADD_FORUM_LIKED' as '@@auth/ADD_FORUM_LIKED',
        likedForum,
    }
}
export const setLocation = (userInfo: IuserInfo) => {
   
    return {
        type: '@@auth/SET_LOCATION' as '@@auth/SET_LOCATION',
        userInfo,       
    }
}

export type IAuthActions = ReturnType<
typeof loginSuccess |
typeof loginFail |
typeof addForumLiked |
typeof loginIsProcessing |
typeof setLocation |
typeof logoutSuccess >



