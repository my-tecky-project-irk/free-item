import { Dispatch } from 'redux';
import { IAuthActions, setLocation } from './actions'
import { loginSuccess, loginFail, loginIsProcessing, addForumLiked } from './actions'
import { CallHistoryMethodAction, push } from 'connected-react-router';
import { logoutSuccess } from './actions'
import { IRootState, ThunkDispatch } from '../store';

const { REACT_APP_API_SERVER } = process.env;

export const restoreLoginThunk = () => {
    return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>, getState: () => IRootState) => {
        const token = localStorage.getItem('token');
      //  console.log('enter restore')
        if (token) {
            const res = await fetch(`${REACT_APP_API_SERVER}/user/info`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });

            if (res.status === 200) {
                const user = await res.json();
                dispatch(loginSuccess({
                    id: user.user_info.id,
                    display_name: user.user_info.display_name,
                    email: user.user_info.username,
                    picture: user.user_info.picture,
                    likedForum: user.user_info.likedForum,
                    lat: user.user_info.lat,
                    lng: user.user_info.lng,
                }));
              //  console.log(getState())
                dispatch(push(getState().router.location.pathname));
            } else {
                dispatch(loginFail(''));
            }
        } else {
            dispatch(loginFail(''));
        }
    }
}

export const loginThunk = (username: string, password: string) => {
    return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>) => {
        dispatch(loginIsProcessing());
       // console.log(REACT_APP_API_SERVER)
        const res = await fetch(`${REACT_APP_API_SERVER}/user/login`, {
            method: 'POST',
            headers: {
                'Content-type': "application/json"
            },
            body: JSON.stringify({ username, password })
        })
        const result = await res.json();


        if (res.status === 200) {
            localStorage.setItem('token', result.token);
            const resV2 = await fetch(`${REACT_APP_API_SERVER}/user/info`, {
                headers: {
                    Authorization: `Bearer ${result.token}`,
                },
            });
            const user = await resV2.json();
          //  console.log(user.user_info)
            dispatch(loginSuccess({
                id: user.user_info.id,
                display_name: user.user_info.display_name,
                email: user.user_info.username,
                picture: user.user_info.picture,
                likedForum: user.user_info.likedForum,
            }));
            dispatch(push('/'))
        } else {
           // console.log('failed')
            const action = loginFail(result.message);
            dispatch(action)
        }
    }
}

export function logoutThunk() {
    return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>) => {
        dispatch(logoutSuccess());
        localStorage.removeItem('token');
        dispatch(push('/'));
    };
}


//Login FB

export function loginFacebook(accessToken: string) {
    return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>) => {
        dispatch(loginIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/user/login/facebook`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({ accessToken })
        })
        const result = await res.json();
        //console.log(result)
        if (res.status !== 200) {
            dispatch(loginFail(result.msg));
        } else {
            localStorage.setItem('token', result.token);
            const resV2 = await fetch(`${REACT_APP_API_SERVER}/user/info`, {
                headers: {
                    Authorization: `Bearer ${result.token}`,
                },
            });
            const user = await resV2.json();
           // console.log(user.user_info)
            dispatch(loginSuccess({
                id: user.user_info.id,
                display_name: user.user_info.display_name,
                email: user.user_info.username,
                picture: user.user_info.picture,
                likedForum: user.user_info.likedForum,
            }));
            dispatch(push('/'));
        }
    }
}

export function loginGoogle(response: any) {
    return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>) => {
        dispatch(loginIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/user/login/google`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({ response })
        })
        const result = await res.json();
   //     console.log(result);
        if (res.status !== 200) {
            dispatch(loginFail(result.msg));
        } else {
            localStorage.setItem('token', result.token);
            const resV2 = await fetch(`${REACT_APP_API_SERVER}/user/info`, {
                headers: {
                    Authorization: `Bearer ${result.token}`
                },
            });
            const user = await resV2.json();
           // console.log(user.user_info)
            dispatch(loginSuccess({
                id: user.user_info.id,
                display_name: user.user_info.display_name,
                email: user.user_info.username,
                picture: user.user_info.picture,
                likedForum: user.user_info.likedForum,
            }));
            dispatch(push('/'));
        }
    }
}

export function forumLikedThunk(user_id: string, discuss_id: string) {
    return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>) => {
      //  console.log(user_id)
       // console.log(discuss_id)
        const res = await fetch(`${REACT_APP_API_SERVER}/user/${user_id}/forumliked/${discuss_id}`)
        const result = await res.json();
      //  console.log(res)
     //   console.log(result)
     //   console.log(result.discuss_id)

        dispatch(addForumLiked(result.discuss_id))
    };

}

export function thunkSetLocation(lat:number, lng:number) {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/user/location`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ lat: lat, lng: lng })
        });
        const json = await res.json();  
       // console.log(json)
        dispatch(setLocation(json.message))
        
    }
}