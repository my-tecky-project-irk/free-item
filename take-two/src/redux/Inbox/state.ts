
export interface Msg {
    id: number;
    room_id: number;
    sender_id: number;
    receiver_id: number;
    is_read: boolean;
    message: string;
    created_at: Date
}

export interface ChatHost {
    id: number;
    host_id: number;
    user_id: number;
    item_id: number;
    display_name: string;
    picture: string;
    message?: string;
    created_at?: Date;
}

export interface ChatUser {
    room_id: number;
    host_id: number;
    user_id: number;
    item_id: number;
    display_name: string;
    picture: string;
    message: string;
    created_at: Date;
}

export interface Item {
    id: number;
    creater_id: number;
    category_id: number;
    title: string;
    intro: string;
    pickup_times: string;
    listing_days: number;
    expire_day: Date;
    is_available: boolean;
    lat: string;
    lng: string;
}

export interface InboxState {
    itemsById: {
        [itemId: string]: Item
    },
    itemsByCreaterId: {
        [creater: string]: number[]
    }

    itemRoomById: {
        [roomId: string]: ChatUser,
    },
    roomByItemId: {
        [host_id: string]: number[],
    },


    userRoomById: {
        [roomId: string]: ChatHost,
    },
    userRoomByItemId: {
        [itemId:string]:number // the number is roomId
    },
    roomByUserId: {
        [user_id: string]: number[],
    },

    msgById: {
        [msgById: string]: Msg,
    },
    allMsgId: number[],
    msgByRoomId: {
        [room_id: string]: number[],
    }
}

export const initialState: InboxState = {
    itemsById: {},
    itemsByCreaterId: {},

    itemRoomById: {},
    roomByItemId: {},

    userRoomById: {},
    userRoomByItemId:{},
    roomByUserId: {},

    msgById: {},
    msgByRoomId: {},
    allMsgId: [],
}
