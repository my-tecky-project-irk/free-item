
import { ThunkDispatch } from "../store";
import { createRoom, loadAllMsgs, loadItemRooms, loadItems, loadUserRooms } from "./action";

export function thunkItems(userId: number) {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/inbox/items`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        const json = await res.json();
        dispatch(loadItems(json, userId))
    }
}

export function thunkItemRooms(itemId: number) {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/inbox/itemrooms/${itemId}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        const json = await res.json();
        dispatch(loadItemRooms(json, itemId))
    }
}

export function thunkUserRooms(userId: number) {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/inbox/userrooms`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        const json = await res.json();  
        dispatch(loadUserRooms(json, userId))
    }
}

export function thunkAllMsgs() {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/inbox/msgs`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })

        const json = await res.json();        
        dispatch(loadAllMsgs(json))
    }
}

export function thunkReadMsg(roomId: number) {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        await fetch(`${process.env.REACT_APP_API_SERVER}/inbox/readmsg/${roomId}`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
    }
}

export function thunkCreateRoom(itemId: number) {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/inbox/createchatroom/${itemId}`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        const json = await res.json();         
        dispatch(createRoom(itemId,json.roomId))
    }
}