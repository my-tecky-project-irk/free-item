import {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { IRootState } from '../store';
import { thunkAllMsgs } from './thunk';

// Custom React hooks
// Group 埋相關嘅 useSelector 同useEffect
// Can read/invalidate cache(快取) 
export function useAllMsg(roomId:number){
    const dispatch = useDispatch();
    const msgIds = useSelector((state: IRootState) => state.inbox.msgByRoomId[roomId]);
    useEffect(() => {
        dispatch(thunkAllMsgs())
    }, [dispatch]);

    return msgIds;
}