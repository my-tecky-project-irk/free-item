


export async function authfetch(url:string){
    const token = localStorage.getItem('token');
    const res = await fetch(url, {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    });

    if(res.status == 400){
        // dispatch some toast showing action

        return null;
    }else if (res.status === 500){
        // dispatch some toast showing action
        return null;
    }else{
        return await res.json();
    }
}