import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { restoreLoginThunk } from "./redux/auth/thunks";
import { IRootState } from "./redux/store";
import { Switch, Route, Redirect } from "react-router-dom";
import LoginPage from "./pages/LoginPage";
import PrivateRoute from "./components/PrivateRoute";
import UserPage from "./pages/UserPage";
import SignUpPage from "./pages/SignUpPage";
import "bootstrap/dist/css/bootstrap.css";
import TakeTwoNav from "./components/Nav";
import ProfilePage from "./pages/ProfilePage";
import ForumPage from "./pages/ForumPage";
import { InboxPage } from "./pages/InboxPage";
import CategoriesPage from "./pages/CategoriesPage";
import ForumChat from "./components/forum/ForumChat";
import UsersProfile from "./components/UsersProfile";

import ItemPage from "./pages/ItemPage";
import SubmitPage from "./pages/SubmitPage";

function App() {
    const dispatch = useDispatch();
    const isAuthenticated = useSelector(
        (state: IRootState) => state.auth.isAuthenticated
    );

    useEffect(() => {
        if (isAuthenticated === null) {
            // dispatch
            dispatch(restoreLoginThunk());
        }
    }, [dispatch, isAuthenticated]);

    return (
        <div className="App">
            <TakeTwoNav />

            <Switch>
                <PrivateRoute path="/" exact={true} component={UserPage} />

                <Route
                    path="/categories/items"
                    exact={true}
                    component={CategoriesPage}
                />
                <Route
                    path="/categories/items/:item_id"
                    exact={true}
                    component={ItemPage}
                />
                <PrivateRoute
                    path="/categories/submit"
                    exact={true}
                    component={SubmitPage}
                />

                <Route path="/forum" exact={true} component={ForumPage} />
                <Route
                    path="/forum/:discuss_id"
                    exact={true}
                    component={ForumChat}
                />

                <PrivateRoute path="/inbox" component={InboxPage} />

                <PrivateRoute
                    path="/profile"
                    exact={true}
                    component={ProfilePage}
                />
                <Route
                    path="/profile/:user_id"
                    exact={true}
                    component={UsersProfile}
                />

                <Route path="/login" exact={true} component={LoginPage} />
                <Route path="/signup" exact={true} component={SignUpPage} />

                <Route><Redirect to="/"/></Route>
            </Switch>
        </div>
    );
}

export default App;
