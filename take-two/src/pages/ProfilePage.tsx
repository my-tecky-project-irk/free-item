import Paper from "@material-ui/core/Paper";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../redux/store";
import styles from "../scss/profile.module.scss";
import "../scss/profile.scss";
import standarduserlogo from "../user.png";
import ProfileForum from "../components/profile/ProfileForum"
import ProfileItem from "../components/profile/ProfileItem"
import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";
import { thunkSetLocation } from "../redux/auth/thunks";
import MyLocationSharpIcon from "@material-ui/icons/MyLocationSharp";
import { Circle } from "@react-google-maps/api";

import { thunkUserShare } from "../redux/items/thunks";
import { Button } from "@material-ui/core";
import cx from 'classnames';

// const {REACT_APP_UPLOAD} = process.env

const ProfilePage = () => {
  const userInfo = useSelector((state: IRootState) => state.auth.userInfo);
  // const content = useSelector((state: IRootState) => state.forum.content);
  //  const categoriesItems = useSelector((state: IRootState) => state.categories.categoriesItems);
  // const itemsId = useSelector((state: IRootState) => state.item.shareItemByUserId[userInfo.id]);
  const dispatch = useDispatch();

  const [mapRef, setMapRef] = useState(null);
  const lat = userInfo.lat ? parseFloat(userInfo.lat) : 22.3;
  const lng = userInfo.lng ? parseFloat(userInfo.lng) : 114;
  const [center, setCenter] = useState({ lat: lat, lng: lng });
  const [position, setPosition] = useState({ lat: lat, lng: lng });


  const options = {
    strokeColor: "#FF0000",
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: "#FF0000",
    fillOpacity: 0.35,
    clickable: false,
    draggable: false,
    editable: false,
    visible: true,
    zIndex: 1,
  };

  function handleCenterChanged() {
    if (!mapRef) return;
    //@ts-ignore
    setPosition(mapRef.getCenter().toJSON());
  }

  function locateMe() {
    navigator.geolocation.getCurrentPosition(
      function (position) {
        //console.log("Latitude is :", position.coords.latitude);
        //console.log("Longitude is :", position.coords.longitude);
        return setCenter({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        });
      },
      function (error) {
        console.error(
          "Error Code = " + error.code + " - " + error.message
        );
      }
    );
  }



  useEffect(() => {
    dispatch(thunkUserShare(userInfo.id));
  }, [dispatch, userInfo.id]);

  return (
    <Paper className={styles.main}>
      <div className={styles.profile_with_map}>
        <Paper className={styles.profilebox}>
          {!userInfo.picture && (
            <img
              src={standarduserlogo}
              alt="userlogo"
              className={styles.profilepicture}
            />
          )}
          {userInfo.picture && (
            <img
              src={userInfo.picture}
              className={styles.profilepicture}
              alt="userlogo"
            />
          )}
          <Paper className={styles.profileinfo}>
            <div className={cx(styles.textbox,styles.font)}>
              Name: {userInfo.display_name}
            </div>
            <div className={styles.textbox + " " + styles.font}>
              Email: {userInfo.email}
            </div>
            <div className={styles.textbox + " " + styles.font}>
              UserID: {userInfo.id}
            </div>
            {/* <div className={styles.textbox + " " + styles.font}>
              No. of Shared item:{" "}
            </div> */}
          </Paper>

        </Paper>
        <Paper className={styles.itembox}>
          <div className={styles.map}>
            <Button variant="outlined" color="secondary"
              onClick={() => dispatch(thunkSetLocation(position.lat, position.lng))}>
              Set Home Location   </Button>
            <Button color="secondary" onClick={() => locateMe()}>
              <MyLocationSharpIcon /> Locate Me
                    </Button>

            {/* AIzaSyDewH0N2PuKdfQlIHawbHHS1ScS1rDuTbI */}
            <LoadScript googleMapsApiKey="AIzaSyBVyGg4eKKvNl4M2utwWorZ3jUdlFdiZZQ">
              <GoogleMap
                onLoad={(map: any) => setMapRef(map)}
                onCenterChanged={handleCenterChanged}
                zoom={15}
                center={center}
                mapContainerStyle={{
                  height: "400px",
                  width: "100%",
                }}
              >
                <Marker position={position} />
                <Circle
                  center={{ lat: lat, lng: lng }}
                  radius={70}
                  options={options}
                />
                {/* {console.log(userInfo)} */}
              </GoogleMap>
            </LoadScript>
          </div>
        </Paper>
      </div>
      <ProfileItem />
      <ProfileForum />
      {/* {categoriesItems[0] && <ProfileItem/>}
            {!categoriesItems[0] && <h1>You have no item shared</h1>} */}
      {/* {content[0] && <ProfileForum/>}
            {!content[0] && <h1>You have no content</h1>} */}

    </Paper>

  );
};

export default ProfilePage;
