import React from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { loginThunk } from '../redux/auth/thunks';
import { IRootState } from '../redux/store';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import GoogleLogin from '../components/auth/GoogleLogin'
import { Box } from '@material-ui/core';
import FBlogin from '../components/auth/FBLogin';
import '../scss/login.scss'
import { push } from 'connected-react-router';
import { Alert } from 'reactstrap';

const { REACT_APP_UPLOAD } = process.env

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href={process.env.HOST}>
        Take Two
        </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  new: {
    margin: theme.spacing(3, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}));

interface IFormInputs {
  username: string;
  password: string;
}

export default function LoginPage() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const isLoginProcessing = useSelector((state: IRootState) => state.auth.isProcessing);
  const { register, handleSubmit, errors } = useForm<IFormInputs>();
  const onSubmit = (data: IFormInputs) => {
   // console.log(data);

    if (!isLoginProcessing) {
   //   console.log('hello')
      dispatch(loginThunk(data.username, data.password));
    }
  };
  return (
    <Grid className="login_main_page">
      <img className="loginImg" src={`${REACT_APP_UPLOAD}/58317.jpg`} alt="" />
      <Grid className="loginInput" >
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Login
            </Typography>
          <form className={classes.form} noValidate onSubmit={handleSubmit(onSubmit)}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="username"
              label="Email Address"
              name="username"
              autoComplete="email"
              autoFocus
              inputRef={register({ required: true, minLength: 1 })}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              inputRef={register({ required: true, minLength: 1 })}
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
              </Button>

            <br />
            <div className='or'>  <span>or</span>    </div>
            <br />
            <FBlogin />
            <br />
            <GoogleLogin />
            <div></div>
            {errors.username && <Alert color="danger" style={{ marginTop: 15 }} >{errors.username && "Please enter username"}</Alert>}
            {errors.password && <Alert color="danger" >{errors.password && "Please enter password"}</Alert>}
            <br /><br />
            <Grid container>
              <Grid item xs>
              </Grid>
              <Grid item>
                <Button variant="contained" color="secondary" onClick={() => dispatch(push("/signup"))}>REGISTER</Button>

              </Grid>
            </Grid>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>

  )
}

