
import React, { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import { ChatToUser } from '../components/inbox/ChatToUser';
import '../scss/inbox.scss'
import { InboxNav } from '../components/inbox/InboxNav';
import { ChatToHost } from '../components/inbox/ChatToHost';
import { thunkAllMsgs } from '../redux/Inbox/thunk';
import { useDispatch } from 'react-redux';
import { CreateChat } from '../components/inbox/CreateChat';



export function InboxPage() {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(thunkAllMsgs())
    }, [dispatch]);

    return (
        <div className='inbox-page'>
            <InboxNav />

            <Switch>
                <Route path='/inbox/request/:id/room' exact> <div className='inbox-right'></div></Route>
                <Route path='/inbox/share/:id/room' exact> <div className='inbox-right'></div></Route>
                <Route path='/inbox/share/:id/room/:id' ><ChatToUser /></Route>
                <Route path='/inbox/request/:id/room/:id'><ChatToHost /></Route>
                <Route path='/inbox/request/:id/item/:id'><CreateChat /></Route>
            </Switch>
        </div>)
}




