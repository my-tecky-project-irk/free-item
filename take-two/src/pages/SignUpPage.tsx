import React, { useEffect, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useForm } from 'react-hook-form';
import { NavLink } from 'react-router-dom';
import { Alert } from 'reactstrap';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router';


const {REACT_APP_API_SERVER} = process.env;
const {REACT_APP_UPLOAD} = process.env;

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href={process.env.HOST}>
        Take Two
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

interface IFormInputs{
  display_name: string;
  username: string;
  password: string;
  re_password: string;
  picture: string;
}

export default function SignUp() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [registerFailMessage, setRegisterFailMessage] =useState("")
  const [registerSuccessMessage, setRegisterSuccessMessage] =useState("")
  const {register, handleSubmit} = useForm<IFormInputs>();
  const onSubmit = async (data: IFormInputs)=> {
    data.picture = `${REACT_APP_UPLOAD}/frog.jpg`
      const res = await fetch(`${REACT_APP_API_SERVER}/user/register`,{
          method: 'POST',
          headers:{
              "Content-Type":"application/json; charset=utf-8"
          },
          body: JSON.stringify({data})
      })
      const result = await res.json();
   //   console.log(result.msg)
   
      if(res.status !==200) {
        setRegisterFailMessage(result.msg)
    } else {
      setRegisterSuccessMessage(result.msg)
      setTimeout( () => ( 
        dispatch(push("/login")))
        , 5000)
    }
  }
 
    useEffect(
      () => {
        let timer1 = setTimeout(() => setRegisterFailMessage(""), 5000)
        return () => {
          clearTimeout(timer1)
        }
      },
      [registerFailMessage])
  
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit(onSubmit)}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                autoComplete="fname"
                name="display_name"
                variant="outlined"
                required={true}
                fullWidth
                id="firstName"
                label="Username"
                autoFocus
                inputRef={register}
                
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required={true}
                fullWidth
                id="email"
                label="Email Address"
                name="username"
                autoComplete="email"
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required={true}
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required={true}
                fullWidth
                name="re_password"
                label="Re-enter Password"
                type="password"
                id="password"
                autoComplete="current-password"
                inputRef={register}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label="I want to receive inspiration, marketing promotions and updates via email."
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
            <NavLink to="/login" >
             Already have an account? Sign in
            </NavLink>
            
            </Grid>
          </Grid>
        </form>
        {registerFailMessage !== "" && <Alert color="danger">{registerFailMessage}</Alert>}
        {registerSuccessMessage !== "" && <Alert color="success">{registerSuccessMessage}</Alert>}
        
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
}