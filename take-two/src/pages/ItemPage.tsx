import { Button, Paper } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import styles from "../scss/itemID.module.scss";
import useReactRouter from "use-react-router";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../redux/store";
import { Circle, GoogleMap, LoadScript } from "@react-google-maps/api";
import "../css/ItemPageGoogleMap.css";
import { thunkCreateRoom } from "../redux/Inbox/thunk";
import { push } from "connected-react-router";
import { thunkImages, thunkItemInfo } from "../redux/items/thunks";
import moment from "moment";
import LocationOnIcon from '@material-ui/icons/LocationOn';
import MyLocationSharpIcon from "@material-ui/icons/MyLocationSharp";
import Carousel from 'nuka-carousel';

import '../scss/itemPage.scss'
const haversine = require('haversine')


// const { REACT_APP_API_SERVER } = process.env;

// interface IitemInfo {
//   title: string;
//   id: string;
//   intro: string;
//   pickup_times: string;
//   listing_days: string;
//   is_available: string;
//   image: string;
//   created_at: Date;
//   lat: number;
//   lng: number;
//   category: string;
//   display_name: string;
//   picture: string;
//   user_id: string;
//   username: string;
// }

const options = {
	strokeColor: "#FF0000",
	strokeOpacity: 0.8,
	strokeWeight: 2,
	fillColor: "#FF0000",
	fillOpacity: 0.35,
	clickable: false,
	draggable: false,
	editable: false,
	visible: true,
	zIndex: 1,
};

const ItemPage = () => {
	const dispatch = useDispatch();
	const userInfo = useSelector((state: IRootState) => state.auth.userInfo);
	const router = useReactRouter<{ item_id: string }>();
	const item_id = router.match.params.item_id;
	const itemInfo = useSelector((state: IRootState) => state.item.itemInfoById[item_id]);
	const imgId = useSelector((state: IRootState) => state.item.imgByItemId[item_id]);
	const imgs = useSelector((state: IRootState) => imgId?.map((id) => state.item.imgById[id]));

	let end: { latitude: number | null; longitude: number | null; } = { latitude: null, longitude: null };
	if (itemInfo) {
		end = { latitude: parseFloat(itemInfo.lat), longitude: parseFloat(itemInfo.lng) }
	}

	let init: { latitude: number | null; longitude: number | null; } = { latitude: null, longitude: null };
	if (userInfo.lat && userInfo.lng) {
		init = {
			latitude: parseFloat(userInfo.lat), longitude: parseFloat(userInfo.lng)
		}
	}
	const [start, setStart] = useState({ latitude: init.latitude, longitude: init.longitude })
	const [dis, setDis] = useState(haversine(start, end))


	useEffect(() => {
		if (itemInfo) {
			setDis(haversine(start, end))
		}
	}, [start, end, itemInfo])

	useEffect(() => {
		dispatch(thunkImages(parseInt(item_id)));
	}, [item_id, dispatch]);

	useEffect(() => {
		dispatch(thunkItemInfo(parseInt(item_id)));
	}, [dispatch, item_id]);

	useEffect(() => {
		dispatch(thunkImages(parseInt(item_id)));
	}, [dispatch, item_id]);

	function locateMe() {
		navigator.geolocation.getCurrentPosition(
			function (position) {
				//console.log("Latitude is :", position.coords.latitude);
				//console.log("Longitude is :", position.coords.longitude);   

				setStart({
					latitude: position.coords.latitude,
					longitude: position.coords.longitude,
				})
				// console.log(start, end)
				// console.log(haversine(start, end))
				setDis(haversine(start, end))
				return
			},
			function (error) {
				console.error(
					"Error Code = " + error.code + " - " + error.message
				);
			}
		);
	}

	return (
		<>
			{itemInfo && (
				<div className={styles.main}>
					{/* <button
						onClick={() => {
							history.goBack();
						}}
						className={styles.returnButton}
					>
						<RiArrowGoBackLine className={styles.arrow} />
					</button> */}
					<Paper className={styles.paper}>
						<div className={styles.item_photo_and_detail}>
							{/* <div className='img-container'> */}
							<Carousel className='img-container'>
								{imgs && imgs.map(img => <>
									<img className={styles.photo}
										src={`${process.env.REACT_APP_UPLOAD}/${img.path}`} alt="" />
								</>)}
							</Carousel>

							{/* </div> */}
							<div className={styles.detail}>
								<div className={styles.header}>
									<div className={styles.post_user}>
										<img
											className={styles.icon}
											src={itemInfo?.picture}
											alt="user_logo"
										/>
										<div>{itemInfo.display_name}</div>
									</div>
									{userInfo.id > 0 && userInfo.id !== itemInfo.user_id && (
										<Button
											className={styles.buttonSize}
											color="primary"
											variant="outlined"
											onClick={() => {
												dispatch(thunkCreateRoom(parseInt(item_id)));
												const func = () =>
													dispatch(
														push(
															`/inbox/request/${userInfo.id}/item/${item_id}`
														)
													);
												func();
											}}
										>
											Request This
										</Button>
									)}
									{/* <button
										onClick={() => {
											history.goBack();
										}}
										className={styles.returnButton}
									>
										<RiArrowGoBackLine className={styles.arrow} />
									</button> */}
								</div>
								<hr />
								<div className={styles.title}>{itemInfo.title}</div>
								<div className={styles.description}>{itemInfo.category}</div>
								<div className={styles.description}>{itemInfo.intro}</div>
								<div className={styles.description}>
									Pickup Times:
                  <div>{itemInfo.pickup_times}</div>
								</div>
								<div className={styles.description}>
									Approx. Location:
                  {<Button color="secondary" onClick={() => { locateMe() }}>
										<MyLocationSharpIcon /> Locate Me
                    </Button>}
									{itemInfo && start.latitude ? <div> <LocationOnIcon />
										{dis.toFixed(2)} KM away</div> : ''}
								</div>
								<div className={styles.description}>
									Expire on : {moment(itemInfo?.expire_day).format('DD-MM-YYYY')}
								</div>
								<div className={styles.description}>
									Created at: {moment(itemInfo?.created_at).format('DD-MM-YYYY')}</div>
								<div className={styles.description}>
									Remember to have safe pick ups during COVID-19!!!
                				</div>
								<div className={styles.description}>
									Posted by {itemInfo?.display_name}
								</div>
								<div></div>
							</div>

						</div>

						<div className={styles.googleMapCenter}>
							<div className="googleMap">
								<LoadScript googleMapsApiKey="AIzaSyBVyGg4eKKvNl4M2utwWorZ3jUdlFdiZZQ">
									<GoogleMap
										zoom={15}
										center={{
											lat: Number(itemInfo.lat), // 22.3203648,
											lng: Number(itemInfo.lng), // 114.169773,
										}}
										mapContainerStyle={{
											height: "400px",
											width: "100%",
										}}
									>
										<Circle
											center={{
												lat: Number(itemInfo.lat),
												lng: Number(itemInfo.lng),
											}}
											radius={70}
											options={options}
										/>
										{/* <Marker
                      position={{
                        lat: Number(itemInfo.lat),
                        lng: Number(itemInfo.lng),
                      }} 
                    />*/}
									</GoogleMap>
								</LoadScript>
							</div>
						</div>
					</Paper>
				</div>
			)
			}
		</>
	);
};

export default ItemPage;
