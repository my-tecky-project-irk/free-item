import { Button, FormControl, FormGroup, InputLabel, MenuItem, Paper, Select, TextField } from '@material-ui/core'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import styles from '../scss/forum.module.scss'
import { IRootState } from '../redux/store'
import { addSingleDiscussionThunk, getAllDiscussionThunk } from '../redux/forum/thunks'

import Input from "reactstrap/lib/Input";

import ForumComponent from '../components/forum/ForumComponent'
import { Alert } from 'reactstrap';

export interface IFormInputs {
  user_id: number | null;
  title: string;
  category: string;
  description: string;
  hashtag: any;
  image: FileList;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    root: {
      "& > *": {
        margin: theme.spacing(1),
        width: "95%",
       
      },
    },
  })
);

export default function ForumPage() {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated)
  const content = useSelector((state: IRootState) => state.forum.content)
  const userInfo = useSelector((state: IRootState) => state.auth.userInfo)
  const [category, setCategory] = useState('');
  const [checkCategory, setCheckCategory] = useState(false);
  const [submitSuccess, setSubmitSuccess] = useState(false);
  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setCategory(event.target.value as string);
  };
  const classes = useStyles();
  const { register, handleSubmit , reset , errors, clearErrors } = useForm<IFormInputs>();
  const [state, setState] = useState({ tags: ["Tags", "Input"] })
  // const [image, setImage] = useState<undefined | File>(undefined);

  const removeTag = (i: any) => {
    const newTags = [...tags];
    newTags.splice(i, 1);
    setState({ tags: newTags });
  }
  const { tags } = state;
  const inputKeyDown = (event: any) => {
    const val = event.target.value;
    if (event.key === 'Enter' && val) {
      event.preventDefault();
      if (tags.find(tag => tag.toLowerCase() === val.toLowerCase())) {
        return;
      }
      event.preventDefault();
      setState({ tags: [...tags, val] });
      event.target.value = null;
    } else if (event.key === 'Backspace' && !val) {
      removeTag(tags.length - 1);
    }
  }
  // setProcessing
  const onSubmit = (data: IFormInputs) => {
    if(category !== ""){
      clearErrors()
      data["user_id"] = userInfo.id
      data["category"] = category
      data["hashtag"] = state.tags
    
      dispatch(addSingleDiscussionThunk(userInfo, data))
      reset({
        title: "",
        description: "",
        hashtag: "",
        image: undefined,
        })
      setCategory("")
      setState({ tags: ["Tags", "Input"] })
      setCheckCategory(false)
      setSubmitSuccess(true)
      setTimeout( () => ( 
        setSubmitSuccess(false))
        , 5000)
    } else {
      setCheckCategory(true)
    }
  };

  useEffect(() => {
    dispatch(getAllDiscussionThunk(userInfo.id));
  }, [dispatch, userInfo]);

  return (
    <div>
      <div className={styles.main}>
        {isAuthenticated && <Paper className={styles.submitbox}>
          <form onSubmit={handleSubmit(onSubmit)} className={classes.root + ' ' + styles.form} noValidate autoComplete="off">
            <TextField
              id="standard-secondary"
              name="title"
              label="Title"
              color="secondary"
              variant="outlined"
              inputRef={register({ required: true, minLength: 1})}
            />
            <div></div>
            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel id="demo-simple-select-outlined-label">Categories</InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={category}
                onChange={handleChange}
                label="Category"
              >
             
                <MenuItem value={'7'}>Q&A</MenuItem>
                <MenuItem value={'9'}>Events</MenuItem>
                <MenuItem value={'11'}>Food Rescue</MenuItem>
                <MenuItem value={'8'}>Tips & Tricks</MenuItem>
                <MenuItem value={'10'}>Announcement</MenuItem>
                <MenuItem value={'6'}>Other</MenuItem>
              </Select>
            </FormControl>

            <div></div>
            <TextField
              id="filled-secondary"
              name="description"
              label="What's on your mind?"
              variant="outlined"
              color="secondary"
              multiline
              rows={5}
              rowsMax={7}
              inputRef={register({ required: true, minLength:1  })}
            />
            <div></div>
            <FormGroup>
              <Input
                type="file"
                name="image"
                id="exampleFile"
                accept="image/*"
                innerRef={register}
       
              />
            </FormGroup>
            <div></div>
            <TextField
              id="filled-secondary"
              name="Hashtag"
              label="Hashtag"
              variant="outlined"
              color="secondary"
              onKeyDown={inputKeyDown}
            />
            <div>
              <ul className={styles.input_tag__tags}>
                {tags.map((tag, i) => (
                  <li key={tag}>
                    {tag}
                    <button type="button" onClick={() => { removeTag(i); }}>+</button>
                  </li>
                ))}
                {/* <li className="input-tag__tags__input"><TextField variant="outlined" placeholder="Add HashTag Here" type="text" onKeyDown={inputKeyDown}/></li> */}
              </ul>
            </div>
            <div>
              <Button style={{ marginRight: 15 }} color="primary"  type='submit' variant="outlined" >Submit</Button>

            </div>
          </form>
        {errors.title && <Alert color="danger">{errors.title && "Please enter title"}</Alert>}
        {errors.description && <Alert color="danger">{errors.description && "Please enter description"}</Alert>}
        {checkCategory && <Alert color="danger">Please choose Categories</Alert>}
        {submitSuccess && <Alert color="success">Submit Successfully!</Alert>}

        </Paper>}

        {content && content.length > 0 && (
          <Paper className={styles.itembox}>
            {content.map((item, idx) => (
              <ForumComponent
                discuss_id={item.discuss_id}
                picture={item.picture}
                display_name={item.display_name}
                created_at={item.created_at}
                title={item.title}
                category={item.category}
                description={item.description}
                hash_tag={item.hash_tag}
                data={item.data}
                image={item.image}
              />))}
          </Paper>)}
      </div>
    </div>
  )
}
