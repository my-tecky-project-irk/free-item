import React, { useState } from "react";
// import { makeStyles, Theme } from "@material-ui/core/styles";

import Tab from "@material-ui/core/Tab";
import logo from "./logo.png";
import AppsIcon from "@material-ui/icons/Apps";
import ForumIcon from "@material-ui/icons/Forum";
import LibraryAddIcon from "@material-ui/icons/LibraryAdd";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { useDispatch, useSelector } from "react-redux";
import { push } from "connected-react-router";
import { IRootState } from "../redux/store";
import { logoutThunk } from "../redux/auth/thunks";
import { Link } from "react-router-dom";
import "../scss/Nav.scss";

import standarduserlogo from "../user.png";
import { InboxBadge } from "./inbox/InboxBadge";
//@ts-ignore
import { Avatar } from "react-chat-elements";
import {
    Collapse,
    Navbar,

    NavbarBrand,
    NavbarToggler,
    Nav,

} from "reactstrap";


// const {REACT_APP_UPLOAD} = process.env
// const useStyles = makeStyles((theme: Theme) => ({
//     root: {
//         flexGrow: 1,
//         width: "100%",
//         backgroundColor: theme.palette.background.paper,
//     },
// }));

export default function TakeTwoNav() {
    const dispatch = useDispatch();
    const userInfo = useSelector((state: IRootState) => state.auth.userInfo);
    const isAuthenticated = useSelector(
        (state: IRootState) => state.auth.isAuthenticated
    );

    const logout = () => {
        dispatch(logoutThunk());
    };
    // const classes = useStyles();

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <>
            {/* <div className={classes.root}> */}
            {/* <AppBar position="static" color="default"> */}
            {/* <div>
                    <Link to="/">
                        <img className="logo" src={logo} alt="logo" />
                    </Link>
                </div>
                <Tabs
                    className="nav-options"
                    variant="scrollable"
                    scrollButtons="on"
                    indicatorColor="primary"
                    textColor="primary"
                    aria-label="scrollable force tabs example"
                >
                    <Tab
                        label="Listings"
                        icon={<AppsIcon />}
                        onClick={() => dispatch(push("/categories/items"))}
                    />
                    <Tab
                        label="Forum"
                        icon={<ForumIcon />}
                        onClick={() => dispatch(push("/forum"))}
                    />
                    {isAuthenticated && (
                        <Tab
                            label="Add Items"
                            icon={<LibraryAddIcon />}
                            //  to="/categories/SubmitPage" component={Link}
                            onClick={() => dispatch(push("/categories/submit"))}
                        />
                    )}
                    {isAuthenticated && <InboxBadge />}
                </Tabs>
                <Tabs className="nav-end">
                    {isAuthenticated && (
                        <div
                            className="profile"
                            onClick={() => dispatch(push("/profile"))}
                        >
                            {!userInfo.picture && (
                                <Avatar
                                    src={standarduserlogo}
                                    alt={"userlogo"}
                                    size="xsmall"
                                    type="circle flexible"
                                />
                            )}
                            {userInfo.picture && (
                                <Avatar
                                    src={userInfo.picture}
                                    alt={"userlogo"}
                                    size="xsmall"
                                    type="circle flexible"
                                />
                            )}
                            <div className="greet">
                                Hello, {userInfo.display_name}
                            </div>
                        </div>
                    )}

                    {!isAuthenticated && (
                        <Tab
                            label="Login / Register"
                            icon={<ExitToAppIcon />}
                            className="login"
                            component={Link}
                            to="/login"
                        />
                    )}
                    {!isAuthenticated && (
                        <Tab
                            label="Register"
                            icon={<ExitToAppIcon />}
                            className="login"
                            component={Link}
                            to="/signup"
                        />
                    )}
                    {isAuthenticated && (
                        <Tab
                            label="Logout"
                            icon={<ExitToAppIcon />}
                            className="login"
                            onClick={logout}
                        />
                    )} */}
            {/* </Tabs> */}
            {/* </AppBar> */}

            <Navbar color="light" light expand="md">
                <NavbarBrand href="/">
                    <div className="logo">
                        <Link to="/">
                            <img className="logo" src={logo} alt="logo" />
                        </Link>
                    </div>
                </NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto secondNav" navbar>
                        <div className="childrenDiv">
                            <Tab
                                label="Listings"
                                icon={<AppsIcon />}
                                onClick={() =>
                                    dispatch(push("/categories/items"))
                                }
                            />
                        </div>

                        {/* <NavItem>
                            <NavLink
                                onClick={() =>
                                    dispatch(push("/categories/items"))
                                }
                            >
                                <AppsIcon />
                                Listings
                            </NavLink>
                        </NavItem> */}
                        <div className="childrenDiv">
                            <Tab
                                label="Forum"
                                icon={<ForumIcon />}
                                onClick={() => dispatch(push("/forum"))}
                            />
                        </div>
                        <div className="childrenDiv">
                            {isAuthenticated && (
                                <Tab
                                    label="Add Items"
                                    icon={<LibraryAddIcon />}
                                    //  to="/categories/SubmitPage" component={Link}
                                    onClick={() =>
                                        dispatch(push("/categories/submit"))
                                    }
                                />
                            )}
                        </div>
                        <div className="childrenDiv">
                            {isAuthenticated && <InboxBadge />}
                        </div>
                        {/* <div className="childrenDiv">
                            {!isAuthenticated && (
                                <Tab
                                    label="Login / Register"
                                    icon={<ExitToAppIcon />}
                                    className="login"
                                    component={Link}
                                    to="/login"
                                />
                            )}
                            {isAuthenticated && (
                                <Tab
                                    label="Logout"
                                    icon={<ExitToAppIcon />}
                                    className="login"
                                    onClick={logout}
                                />
                            )}
                        </div>

                        {isAuthenticated && (
                            <div
                                className="profile"
                                onClick={() => dispatch(push("/profile"))}
                            >
                                {!userInfo.picture && (
                                    <Avatar
                                        src={standarduserlogo}
                                        alt={"userlogo"}
                                        size="xsmall"
                                        type="circle flexible"
                                    />
                                )}
                                {userInfo.picture && (
                                    <Avatar
                                        src={userInfo.picture}
                                        alt={"userlogo"}
                                        size="xsmall"
                                        type="circle flexible"
                                    />
                                )}
                                <div className="greet">
                                    Hello, {userInfo.display_name}
                                </div>
                            </div>
                        )} */}
                    </Nav>
                    <div className="childrenDiv">
                        {!isAuthenticated && (
                            <Tab
                                label="Login / Register"
                                icon={<ExitToAppIcon />}
                                className="login"
                                component={Link}
                                to="/login"
                            />
                        )}
                        {isAuthenticated && (
                            <Tab
                                label="Logout"
                                icon={<ExitToAppIcon />}
                                className="login"
                                onClick={logout}
                            />
                        )}
                    </div>

                    {isAuthenticated && (
                        <div
                            className="profile"
                            onClick={() => dispatch(push("/profile"))}
                        >
                            {!userInfo.picture && (
                                <Avatar className="user_icon"
                                    src={standarduserlogo}
                                    alt={"userlogo"}
                                    size="small"
                                    type="circle flexible"
                                />
                            )}
                            {userInfo.picture && (
                                <Avatar className="user_icon"
                                    src={userInfo.picture}
                                    alt={"userlogo"}
                                    size="small"
                                    type="circle flexible"
                                />
                            )}
                            <div className="greet">
                                Hello, {userInfo.display_name}
                            </div>
                        </div>
                    )}
                </Collapse>
            </Navbar>
            {/* </div> */}
        </>
    );
}
