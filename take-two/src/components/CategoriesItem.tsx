import React, { useEffect } from "react";
import "../css/CategoriesItem.css";
import ChatIcon from "@material-ui/icons/Chat";
//import FavoriteIcon from "@material-ui/icons/Favorite";
import { useDispatch, useSelector } from "react-redux";
import { push } from "connected-react-router";
import { thunkImages } from "../redux/items/thunks";
import { IRootState } from "../redux/store";
import { thunkCreateRoom } from "../redux/Inbox/thunk";

interface ICategoriesItemProps {
    id: number;
    //  image?: string;
    display_name: string;
    picture: string;
    //  createrId:number;
}

const CategoriesItem: React.FC<ICategoriesItemProps> = ({
    id,
    //   image,
    display_name,
    picture,

}) => {
  
    const dispatch = useDispatch();
    const userInfo = useSelector((state: IRootState) => state.auth.userInfo);
    // should be small letter
    const ClickHandler = () => dispatch(push(`/categories/items/${id}`));
    const imgId = useSelector(
        (state: IRootState) => state.item.imgByItemId[id]
    );
    const imgs = useSelector((state: IRootState) =>
        imgId?.map((id) => state.item.imgById[id])
    );

    useEffect(() => {
        dispatch(thunkImages(id));
    }, [dispatch, id]);

    return (
        <>
            <div className="fullBody">
                <div className="itemImage" onClick={() => ClickHandler()}>
                    <img
                        src={`${process.env.REACT_APP_UPLOAD}/${imgs ? imgs[0].path : "logo.png"
                            }`} alt=""
                    />
                </div>
                <div className="user">
                    <div>
                        <img className="user_icon" src={picture} alt=""/>
                    </div>
                    <div className="user_name">{display_name}</div>
                </div>               
                    <button className="chatButton" onClick={() => {
                        dispatch(thunkCreateRoom(id));
                        const func = () =>
                            dispatch(push(`/inbox/request/${userInfo.id}/item/${id}`));
                        func();
                    }} >
                        <ChatIcon style={{ color: "white" }}  /></button>

                {/* <div className="favButton">
                    <FavoriteIcon style={{ color: "white" }} />
                </div> */}
            </div>
        </>
    );
};

export default CategoriesItem;
