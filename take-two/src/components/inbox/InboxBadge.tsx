import { push } from 'connected-react-router';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { thunkAllMsgs } from '../../redux/Inbox/thunk';
import { Badge, Tab } from '@material-ui/core';
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import { IRootState } from '../../redux/store';



export function InboxBadge() {
    const dispatch = useDispatch();
    const userId = useSelector((state: IRootState) => state.auth.userInfo.id);
    const allMsgId = useSelector((state: IRootState) => state.inbox.allMsgId);
    const allMsg = useSelector((state: IRootState) => allMsgId?.map(id => state.inbox.msgById[id]))

    useEffect(() => {
        dispatch(thunkAllMsgs())
    }, [dispatch]);

    let count = 0
    for (const msg of allMsg) {
        if (msg.receiver_id === userId) {
            if (msg.is_read === false) { count++; }
        }
    }

    return (<>
        <Tab
            label="Inbox"
            icon={
                <Badge color="secondary" badgeContent={count}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                >
                    <MailOutlineIcon />
                </Badge>}
            onClick={() =>
                dispatch(push(`/inbox/share/${userId}/room`))
            }
        />
    </>)
}

