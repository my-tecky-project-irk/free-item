import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store'
import { useRouteMatch } from 'react-router-dom';
import { thunkAllMsgs, thunkUserRooms, } from '../../redux/Inbox/thunk';
import { MyRequestList } from './MyRequesList';

export function MyRequest() {
    const dispatch = useDispatch();
    const match = useRouteMatch<{ id: string }>();
    const userId = parseInt(match.params.id);
    const userRoomsId = useSelector((state: IRootState) => state.inbox.roomByUserId[userId]);
    const userRooms = useSelector((state: IRootState) => userRoomsId?.map(id => state.inbox.userRoomById[id]));

    useEffect(() => {
        dispatch(thunkUserRooms(userId))
    }, [userId, dispatch])

    useEffect(() => {
        dispatch(thunkAllMsgs())
    }, [dispatch]);


    return (<>
        {userRooms && userRooms?.map((room,i )=> (
            <MyRequestList key={i} roomId={room.id}/>
        ))}
    </>)
}




