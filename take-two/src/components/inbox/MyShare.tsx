import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store'
import { useRouteMatch } from 'react-router-dom';
import { thunkAllMsgs, thunkItems } from '../../redux/Inbox/thunk';
import { MyShareItems } from './MyShareItem';

export function MyShare() {
    const dispatch = useDispatch();
    const match = useRouteMatch<{ id: string }>();
    const userId = parseInt(match.params.id)
    const itemsId = useSelector((state: IRootState) => state.inbox.itemsByCreaterId[userId]);
    const items = useSelector((state: IRootState) => itemsId?.map(id => state.inbox.itemsById[id]))

    useEffect(() => {
        dispatch(thunkItems(userId))
    }, [userId, dispatch])

    useEffect(() => {
        dispatch(thunkAllMsgs())
    }, [dispatch]);

    return (<>
        {items?.map((item,i )=> (<div key={i}>
            <MyShareItems itemId={item.id}/>
        </div>))}
    </>)
}









