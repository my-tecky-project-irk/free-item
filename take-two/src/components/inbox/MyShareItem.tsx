
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store'
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { useRouteMatch } from 'react-router-dom';
import WallpaperRoundedIcon from '@material-ui/icons/WallpaperRounded';
import { thunkAllMsgs, thunkItems } from '../../redux/Inbox/thunk';
import { Collapse } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import { MyShareRooms } from './MyShareRooms';
import { thunkImages } from '../../redux/items/thunks';
import styles from '../../scss/myShareItem.module.scss'

export function MyShareItems(props:{
    itemId:number
}) {
    const dispatch = useDispatch();
    const match = useRouteMatch<{ id: string }>();
    const userId = parseInt(match.params.id);   
    const item = useSelector((state: IRootState) => state.inbox.itemsById[props.itemId]);
    const imgId = useSelector((state: IRootState) => state.item.imgByItemId[props.itemId])
    const imgs = useSelector((state: IRootState) => imgId?.map(id => state.item.imgById[id]))

    useEffect(() => {
        dispatch(thunkItems(userId))
    }, [userId, dispatch])

    const [open, setOpen] = React.useState(true);
    const handleClick = () => {
        setOpen(!open);
    };

    useEffect(() => {
        dispatch(thunkAllMsgs())
    }, [dispatch]);

    useEffect(() => {
        dispatch(thunkImages(props.itemId))
    }, [dispatch, props.itemId]);


    return (<>
  
            <ListItem  button onClick={handleClick}>
                <ListItemIcon>
                {imgs? <img className={styles.itemIcon} src={`${process.env.REACT_APP_UPLOAD}/${imgs[0].path}`} alt="" /> : < WallpaperRoundedIcon />}
                </ListItemIcon>
                <ListItemText primary={item.title} />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse  in={open} timeout="auto" unmountOnExit>
                <MyShareRooms itemId={item.id} />
            </Collapse>
    
    </>)
}
