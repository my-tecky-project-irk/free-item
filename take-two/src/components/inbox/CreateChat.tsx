import React, { useEffect, useRef } from 'react';
import 'react-chat-elements/dist/main.css';
// @ts-ignore 
import { MessageBox, Avatar } from 'react-chat-elements';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router-dom';
import { IRootState } from '../../redux/store';
import { thunkAllMsgs, thunkReadMsg, thunkUserRooms } from '../../redux/Inbox/thunk';
import { useFormState } from 'react-use-form-state';
import { Msg } from '../../redux/Inbox/state';
import { readMsg, sendMsg } from '../../redux/Inbox/action';
import { socket } from '../../socket';

import { thunkAllItem, thunkImages } from '../../redux/items/thunks';
import SendIcon from "@material-ui/icons/Send";


export function CreateChat() {
   
    const [formState, { text }] = useFormState();
    const dispatch = useDispatch();
    const match = useRouteMatch<{ id: string }>();
    const itemId = match.params.id;
    const roomId = useSelector((state: IRootState) => state.inbox.userRoomByItemId[itemId]);
    const msgId = useSelector((state: IRootState) => state.inbox.msgByRoomId[roomId]);
    const msgs = useSelector((state: IRootState) => msgId?.map(id => state.inbox.msgById[id]));
    const userId = useSelector((state: IRootState) => state.auth.userInfo.id);
    const rcvr = useSelector((state: IRootState) => state.inbox.userRoomById[roomId]);
    const imgId = useSelector((state: IRootState) => state.item.imgByItemId[itemId]);
    const imgs = useSelector((state: IRootState) => imgId?.map((id) => state.item.imgById[id]));
    const item = useSelector((state: IRootState) => state.item.allItemById[itemId]);

    const ref = useRef(null);

    useEffect(() => {
        dispatch(thunkImages(parseInt(itemId)));
    }, [itemId,dispatch]);

    useEffect(() => {
        dispatch(thunkAllMsgs())
    }, [dispatch]);

    useEffect(() => {
        dispatch(thunkUserRooms(userId))
    }, [userId, dispatch]);

    useEffect(() => {
        dispatch(thunkAllItem());
    }, [dispatch]);

    useEffect(() => {
        socket.emit('join_room', roomId);
        const msgListener = (msg: Msg) => { dispatch(sendMsg(msg)) }
        socket.on('new_msg', msgListener)
        const readListener = (msgs: Msg[]) => { dispatch(readMsg(msgs)) }
        socket.on('read_msg', readListener)

        return () => {
            socket.off('read_msg', readListener)
            socket.off('new_msg', msgListener)
            socket.emit('leave_room', roomId)
        }
    }, [roomId, dispatch])

    useEffect(() => {
        const unread = setInterval(() => {
            dispatch(thunkReadMsg(roomId))
          //  console.log('chat to host:' + roomId)
        }, 1000);
        return () => { clearInterval(unread) }
    }, [dispatch, roomId])

    return (
        <div className='inbox-right'>
            <div className='msg-display'>
                <div className='chat-nav'>
                    {imgs&& <img src={`${process.env.REACT_APP_UPLOAD}/${imgs[0].path}`} alt="" />}
                    {rcvr && <div className='rcvr'>
                        {item && <div className='title'>{item.title}</div>}
                        <p>  <Avatar src={rcvr.picture} alt={'receiver-logo'} type="circle flexible" size='small' />
                            <div> {rcvr.display_name}</div></p>
                    </div>}
                </div>

                <div className='chat-room'>
                    {msgs && msgs?.map((msg, i) => (<>
                        <MessageBox key={i}
                            position={msg.sender_id === userId ? 'right' : 'left'}
                            type={'text'}
                            text={msg.message}
                            date={new Date(msg.created_at)}
                            status={msg.is_read ? 'read' : 'received'}
                        /></>
                    ))}
                    <div ref={ref} />
                </div>
            </div>
            <form onSubmit={async event => {
                event.preventDefault()
                const content = formState.values.message;
                formState.reset();
                const token = localStorage.getItem('token');
                await fetch(`${process.env.REACT_APP_API_SERVER}/inbox/send`, {
                    method: 'POST',
                    headers: {
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        msgContent: content,
                        roomId: roomId,
                        rcvId: rcvr.host_id
                    })
                })
            }}>
                <input {...text('message')} required />
                <div className="BBB">
                    <button type="submit" className="sendButton">
                        <SendIcon style={{ color: "white" }} />
                    </button>
                </div>
            </form>


        </div>
    )
}



