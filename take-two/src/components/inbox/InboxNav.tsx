
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store'
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import { NavLink, Route, Switch } from 'react-router-dom';
import { MyShare } from './MyShare';
import { MyRequest } from './MyRequest';
import { thunkAllMsgs } from '../../redux/Inbox/thunk';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            backgroundColor: theme.palette.background.paper,
        },
        nested: {
            paddingLeft: theme.spacing(4),
        },
    }),
);



export function InboxNav() {
    const classes = useStyles();
    const userInfo = useSelector((state: IRootState) => state.auth.userInfo);
    const dispatch = useDispatch();
    
    useEffect(() => {
        dispatch(thunkAllMsgs())
    }, [dispatch]);

    return (<>
        <div className='inbox-left'>
            <List component="nav" className={classes.root} aria-labelledby="nested-list-subheader">
                <div className='inbox-nav'>
                    <div> <NavLink activeClassName='active' to={`/inbox/share/${userInfo.id}/room`}>My Share</NavLink></div>
                    <div> <NavLink to={`/inbox/request/${userInfo.id}/room`}>My Request</NavLink></div>
                </div>

                <Switch>
                    <Route path="/inbox/share/:id"  ><MyShare/></Route>
                    <Route path="/inbox/request/:id"  ><MyRequest /></Route>
                </Switch>

            </List>
        </div>

    </>)
}




