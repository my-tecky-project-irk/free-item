import { push } from 'connected-react-router';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store'
import ListItem from '@material-ui/core/ListItem';
import { useRouteMatch } from 'react-router-dom';
// @ts-ignore 
import { ChatItem, } from 'react-chat-elements'
import { thunkAllMsgs, thunkReadMsg } from '../../redux/Inbox/thunk';
// import { useAllMsg } from '../../redux/Inbox/hooks';



export function MyRequestList(props: {
    roomId: number
}) {
    const dispatch = useDispatch();
    const match = useRouteMatch<{ id: string }>();
    const userId = parseInt(match.params.id);
    const room = useSelector((state: IRootState) => state.inbox.userRoomById[props.roomId]);
    const msgId = useSelector((state: IRootState) => state.inbox.msgByRoomId[props.roomId]);
    const roomMsg = useSelector((state: IRootState) => msgId?.map(id => state.inbox.msgById[id]));
    // const msgIds = useAllMsg(props.roomId);
    let count = 0
    if (roomMsg) {
        for (const msg of roomMsg) {
            if (msg.receiver_id === userId) {
                if (msg.is_read === false) { 
                    count++; 
                }
            }
        }
    }

    useEffect(() => {
        dispatch(thunkAllMsgs())
    }, [dispatch]);
    // <Link to={`/inbox/request/${room.user_id}/room/${room.id}">
    return (<>    
        <ListItem button onClick={() => {
            const func = () => dispatch(push(`/inbox/request/${room.user_id}/room/${room.id}`))
            func();
            dispatch(thunkReadMsg(room.id))
        }}>
            <ChatItem
                avatar={room.picture}
                title={room.display_name}
                subtitle={room.message}
                date={room.created_at ? new Date(room.created_at) : null}
                unread={count} />
        </ListItem>
    </>)
}
