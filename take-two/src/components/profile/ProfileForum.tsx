import { Paper } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { IRootState } from '../../redux/store'
import {getAllDiscussionByUserIDThunk, deleteDiscussionThunk} from '../../redux/forum/thunks'
// import styles from '../../scss/forum.module.scss'
import styles from '../../scss/profileForum.module.scss'

import ForumComponent from '../forum/ForumComponent'
import { AiFillDelete } from 'react-icons/ai'
import { Alert } from 'reactstrap';

const {REACT_APP_API_SERVER} = process.env;


const ProfileForum=()=> {
  const dispatch = useDispatch();
  const userInfo = useSelector((state:IRootState)=> state.auth.userInfo)
  const content = useSelector((state:IRootState)=> state.forum.content)
  const [message, setMessage] = useState<null | string>(null);

  useEffect(()=> {
    dispatch(getAllDiscussionByUserIDThunk(userInfo.id));
  },[dispatch, userInfo.id])



  return (<>
  <div  className={styles.forumTitle}>Forum discussion</div>
  <Paper className={styles.ForumComponent}>
    {message && <div className={styles.deleteAlertCenter}><Alert color="success" className={styles.deleteAlert} >{message}</Alert></div>}
    {(!content[0] && message === null) && <h3>No forum content shared</h3>}
      {content[0] && content.length > 0 && (
    <div className ={styles.itembox}>
      {content.map((item, idx)=> ( 
      <div>
      <button onClick={async ()=> {
        const res = await fetch(`${REACT_APP_API_SERVER}/forum/removediscussion/${item.discuss_id}`, {
          method: 'DELETE'
            })
            if (res.status === 200) {
              setMessage("Your discussion has been deleted")
              dispatch(deleteDiscussionThunk(item.discuss_id))
              setTimeout( () => ( 
                setMessage(null))
                , 5000)
              }
      }}
      className={styles.deleteButton}><AiFillDelete className={styles.delete}/></button>
        <ForumComponent 
          discuss_id ={item.discuss_id}
          picture = {item.picture}
          display_name = {item.display_name}
          created_at = {item.created_at}
          title = {item.title}
          category = {item.category}
          description = {item.description}
          hash_tag = {item.hash_tag}
          data = {item.data}
          image = {item.image}
        />
      </div>
      ))}
      </div>)}
   
    </Paper></>
  )
}

export default ProfileForum
