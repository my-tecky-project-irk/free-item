import React from 'react';
import { RouteProps, Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { IRootState } from '../redux/store';

const PrivateRoute: React.FC<RouteProps> = ({ component, ...rest }) => {
    const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated);
    const Component = component as any;
    if (Component === null) {
        return null;
    }

    if(isAuthenticated === null) {
      return null;
    }

    let render: (props: any) => JSX.Element;
    if (isAuthenticated) {
        render =  (props: any) => <Component {...props} />;
    } else {
        render = (props: any) => (
            <Redirect
                to={{
                    pathname: '/login',
                    state: { from: props.location },
                }}
            />
        );
    } 
    return <Route {...rest} render={render} />;
};

export default PrivateRoute;
