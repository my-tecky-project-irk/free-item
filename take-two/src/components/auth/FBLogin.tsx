import ReactFacebookLogin, { ReactFacebookLoginInfo } from "react-facebook-login";
import React from 'react';

import {loginFacebook} from '../../redux/auth/thunks'
import { useDispatch } from 'react-redux';

import { AiFillFacebook } from "react-icons/ai";

import styles from '../../css/socialMediaButton.module.css'
export default function FBlogin(){

  /*rest of the code*/
  const fBOnCLick = ()=> {
      return null;
  }

  const dispatch = useDispatch();
  const fBCallback = (userInfo: ReactFacebookLoginInfo & { accessToken: string }) => {
      
      if (userInfo.accessToken) {
     
          dispatch(loginFacebook(userInfo.accessToken));
      }
      return null;
  }

  return (

     <div className={styles.button1}>  
        <ReactFacebookLogin 
            appId={process.env.REACT_APP_FACEBOOK_APP_ID || ''}
            autoLoad={false}
            fields="name,email,picture"
            onClick={fBOnCLick}
            callback={fBCallback}
            textButton= "Sign In with Facebook"
            icon={<AiFillFacebook className={styles.size}/>}
            size= 'small'
            />
    </div>
  
      );
}