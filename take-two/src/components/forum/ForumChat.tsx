import { createStyles,  makeStyles,  Theme } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import React, { useEffect, useState } from 'react'
import useReactRouter from 'use-react-router';
import styles from '../../scss/forumchat.module.scss'
import { useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { IdiscussInfo } from '../../redux/forum/state'
import moment from 'moment';

import { addSingleCommentThunk, getAllCommentThunk } from '../../redux/forum/thunks'
import { RiArrowGoBackLine } from 'react-icons/ri';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

const { REACT_APP_API_SERVER } = process.env
const { REACT_APP_UPLOAD} = process.env
export interface Icomment {
  user_id: number | null;
  discuss_id: string;
  comment: string; 
}


const ForumChat = () => {
   const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated)
  const userInfo = useSelector((state: IRootState) => state.auth.userInfo)
  const router = useReactRouter<{ discuss_id: string }>();
  const comments = useSelector((state: IRootState) => state.forum.comments)
  const discuss_id = router.match.params.discuss_id;

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
      },
      root: {
        "& > *": {
          margin: theme.spacing(1),
          width: "95%",
        },
      },
    })
  );

  const classes = useStyles();
  const [discuss, setDiscuss] = useState<IdiscussInfo | undefined>();
  const { register, handleSubmit, reset} = useForm<Icomment>();
  const dispatch = useDispatch();
  const onSubmit = async (data: Icomment) => {
    data["user_id"] = userInfo.id
    data["discuss_id"] = discuss_id
    dispatch(addSingleCommentThunk(data))
    reset({
      comment: "",
    });
  }

  useEffect(() => {

    const initData = async () => {
      const res = await fetch(`${REACT_APP_API_SERVER}/forum/discuss/` + String(discuss_id))
      const json = await res.json()
     // console.log(json)
     // console.log(typeof json.data[0])
      setDiscuss(json.data[0])


      dispatch(getAllCommentThunk(discuss_id))
    }
    initData()
  }, [dispatch, discuss_id])

  return (
    <div className ={styles.container}>
          <Paper className ={styles.smallItemBox + " " + styles.hoverpaper}>
          <button onClick={()=> {history.goBack()}}className={styles.returnButton}><RiArrowGoBackLine className={styles.arrow}/></button>
            <div className={styles.header}>
                <div className={styles.user}>
                  {discuss?.picture && <img className={styles.icon}src={discuss?.picture} alt="user_logo"/>}
                  <div className={styles.main_display_name}>{discuss?.display_name}</div>
                  <div className={styles.time_size}>posted {moment(discuss?.created_at).startOf('minute').fromNow()}</div>
                </div>
                <div className={styles.title_category}>
                  <div className={styles.title}>{discuss?.title}</div>
                  <div  className={styles.category}>{discuss?.category}</div >
                </div>
            </div>
            <div className={styles.image_board}>
            {discuss?.image && <img className={styles.image} src={`${REACT_APP_UPLOAD}/` + discuss?.image} alt=""/>}
            </div>
            <div className={styles.bottom}>
              <div className={styles.tags_comment_heart}>
                    <div className={styles.upper_show_input_tag}>{discuss?.hash_tag.map((tag, i)=> (
                      <div className={styles.show_input_tag}>#{tag}</div>
                      ))}
                    </div>
                   
              </div>
            <div className={styles.description}>{discuss?.description}</div>
            
            {comments.map((item, idx) => (
          <div className={styles.grid_block}>
            <img className={styles.icon} src={item.picture} alt="" />
            <div className={styles.display_name_comment}>
              <div className={styles.font + " " +  styles.display_name}>{item.display_name} Reply:</div>
              <div className={styles.font + " " +  styles.comment}>{item.comment}</div>
            </div>
          </div>
        ))}
     
        {isAuthenticated && <form onSubmit={handleSubmit(onSubmit)} className={classes.root + 'form'} noValidate autoComplete="off">
          <div className={styles.grid_block}>
            <img className={styles.icon} src={userInfo.picture} alt="user_logo" />
            <input className={styles.input} placeholder="Reply"
              name="comment"
              ref={register}
            />
          </div>
        </form>}
            </div>
   
          </Paper>
          

     
      </div>
  )
}

export default ForumChat
