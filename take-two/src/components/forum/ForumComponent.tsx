import {  Paper } from '@material-ui/core'
import React from 'react'
// import { FiHeart } from 'react-icons/fi'
import { useDispatch } from 'react-redux'
// import { forumLikedThunk } from '../../redux/auth/thunks'
// import { IRootState } from '../../redux/store'
import styles from '../../scss/profileForum.module.scss'
import { BsChat } from 'react-icons/bs'
import moment from 'moment'
import { push } from 'connected-react-router'
import { Icomment } from '../../redux/forum/state'


const {REACT_APP_UPLOAD} = process.env

interface IForumComponentProps{
  discuss_id: number;
  picture: string;
  display_name: string;
  created_at: string;
  title: string;
  category: string;
  description: string;
  hash_tag: string[];
  data: Array<Icomment>;
  image: string;
}


const ForumComponent: React.FC<IForumComponentProps>=({
  discuss_id,
  picture,
  display_name,
  created_at,
  title,
  category,
  description,
  hash_tag,
  data,
  image,
})=> {
    const dispatch = useDispatch();
    // const userInfo = useSelector((state: IRootState) => state.auth.userInfo)
  return (

        <div>
          <Paper onClick={()=> {dispatch(push(`/forum/${discuss_id}`))}} className ={styles.smallItemBox + " " + styles.hoverpaper}>
            <div className={styles.header}>
                <div className={styles.user}>
                  {picture && <img className={styles.icon}src={picture} alt=""/>}
                  <div className={styles.main_display_name}>{display_name}</div>
                  <div className={styles.time_size}>posted {moment(created_at).startOf('minute').fromNow()}</div>
                </div>
                <div className={styles.title_category}>
                  <div className={styles.title}>{title}</div>
                  <div  className={styles.category}>{category}</div >
                </div>
            </div>
            {/* <div className={styles.image_board}> */}
               {/* </div> */}
            {image && 
            <div>
              <img className={styles.image} src={`${REACT_APP_UPLOAD}/` + image} alt="" />
              <div className={styles.bottom}>
                <div className={styles.tags_comment_heart}>
                      <div className={styles.upper_show_input_tag}>{hash_tag.map((tag, i)=> (
                        <div className={styles.show_input_tag}>#{tag}</div>
                        ))}
                      </div>
                      <div className={styles.comment}>
                          <div className={styles.commentlength}>{data.length}</div>
                          <BsChat type='submit' className={styles.chat + " " + styles.icon_size}></BsChat>
                          {/* {(userInfo.likedForum)?.includes(discuss_id) && 
                          <FiHeart  className={styles.icon_size + " " + styles.liked} type={`submit`} onClick={ async()=> {
                            dispatch(forumLikedThunk(String(userInfo.id), String(discuss_id)))}}/>}
                          {!(userInfo.likedForum)?.includes(discuss_id) && 
                          <FiHeart className={styles.icon_size} type={`submit`} onClick={ async()=> {
                            dispatch(forumLikedThunk(String(userInfo.id), String(discuss_id)))}}/>}  */}
                      </div>
                </div>
              <div className={styles.description}>{description}</div>
              </div>
            </div>
            }
            {!image && 
            <div>
              <div className={styles.bottom}>
              <div className={styles.description}>{description}</div>
                <div className={styles.tags_comment_heart}>
                      <div className={styles.upper_show_input_tag}>{hash_tag.map((tag, i)=> (
                        <div className={styles.show_input_tag}>#{tag}</div>
                        ))}
                      </div>
                      <div className={styles.comment}>
                          <div className={styles.commentlength}>{data.length}</div>
                          <BsChat type='submit' className={styles.chat + " " + styles.icon_size}></BsChat>
                          {/* {(userInfo.likedForum)?.includes(discuss_id) && 
                          <FiHeart  className={styles.icon_size + " " + styles.liked} type={`submit`} onClick={ async()=> {
                            dispatch(forumLikedThunk(String(userInfo.id), String(discuss_id)))}}/>}
                          {!(userInfo.likedForum)?.includes(discuss_id) && 
                          <FiHeart className={styles.icon_size} type={`submit`} onClick={ async()=> {
                            dispatch(forumLikedThunk(String(userInfo.id), String(discuss_id)))}}/>}  */}
                      </div>
                </div>
              </div>
            </div>
            }
          </Paper>
          
      </div>

  )
}

export default ForumComponent
